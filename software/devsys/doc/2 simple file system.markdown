simple file system
------------------

there are files.
file is named array of zero or more bytes.

there are folders.
folder is named collection of files.

there is special file inside every folder named 'dir'.
'dir' is file which contains list of file names in that folder.

file system is folder named '/'.
there is special folder in file system called 'system'.

for each storage device there is program called driver.
driver have following calls - reset, rewrite, close, read, write, filesize
device accessibility depends on existence of file '/system/'+device name.

/system/dir contains list of all drivers.
/system/system is driver for system

/system/null is driver for null device
/null device can do whatever it want with it's files, every operation in it is successful,
all files in /null are empty
/system/system i /system/null can not be deleted


programs are stored as files in file system.
first n bytes in program file represent checksum of other bytes in file.
programs can be run by 'running file'.
execution of program depends on it's checksum being valid.

shell commands available in editor: create empty, move/rename, copy (delete can be move to /null), run
?mounting and unmounting, storage for drivers

/dir:
/system/
/null/
.

/system/dir:
/system/system/
/system/null/
/system/z80_emu
/system/z80
/system/ram
.

/ram/dir:
.
/z80:
/z80/dir
/z80/emu
/z80/run

copying driver to /system is actually installation of driver for driver's device
