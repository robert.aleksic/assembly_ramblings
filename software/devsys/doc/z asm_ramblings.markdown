
constants, variables, types (arrays, records, pointers, enumerated, reals?)
functions, procedures, memory allocation (in reg, absolute, on stack)

source, destination: mem, reg, imm, offset, indexing, pre and post inc/dec,

min instruction set
library/units concept scopes, locality


move, push pop? // immediate cannot be destination (load / store / registers?)
arithmetic (add, sub w & w/o carry, mul (emulation possible)), multiply with add and subtract, signed, unsigned
logical (not, and, or, xor, shifts and rotations (arithmetic, logical), reversals, masks set/clear)
division or emulation?
flag update on arm?
bit, byte, word, long, quad and emulations?
instruction alignment

bit operations (test and branch)
try to avoid jumps in assembler if possible replace with loops
  loops - while, repeat until eventually for's
calling conventions, parameter passing (usable registers)

conditional models: with instruction and jumps
 ifs and cases, table jumps?

interrupts, watchdogs?
gpio/memory mapped devices

features:
 floating point, massive moves, dma, a/d conversion, memory management units, pwm, timers, event counters, watchdogs, on-chip ram, eeprom etc.

Harvard vs. Von Neumann does it have impact?

communications serial (uarts, rs422, rs232, i2c, spi, usb), irda, serial bus chained devices, wireless, ethernet, parallel?
channel concept in and out memory,
parallelism
  atomic instructions/critical sections
  can interrupts be wrapped in some other parallel concepts

check micro controllers and bit based architectures

tracing / debugging / timing analysis

utf-8 constants, string representation?
lsb vs msb, thumb on arm ...
specific memmory maps?

min-os concept providing running facilities, memory allocation, isolation?, communication, file systems

which processors to target first
  microchip pic, atmel, stm's, arm
general purpose
  arm, intel?



