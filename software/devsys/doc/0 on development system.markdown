Development system
------------------

There is device under development. This device is called target device.
There is device used for development. This device is called development device.

Software used for development is called development system.
Development system consists of editor, assembler and emulator.

In the beginning of development, development system is used on development device.
Whenever possible aim should be to move development system to target device as soon as possible.
This might not be possible in case of extremely light hardware on target device.

Emulator closely mimic behaviour of target device on development device.
From a standpoint of user of development system it should be irrelevant is he running program in emulator or on target device.

Communication
-------------

Physical channel is bi-directional serial link between two devices.
Target device and development device are connected via at least one physical channel.

Logical channel (or simply channel) is uni-directional link between two devices.
Both devices must have ability to send and receive bytes over channels.
Those operations are denoted 'channel ! val' and 'channel ? var'.
Communication take place when both devices are ready for transfer.
Each device can query status of other device via 'channel.any'.
When device try to communicate on channel it is blocked until other party is ready to communicate.

Target device must have at least four channels (ctrl.in, ctrl.out, in and out).
Development device must have at least four channels (ctrl.out, control.in, keyboard and screen).
'ctrl ! val' is short form of 'ctrl.out ! val'
'ctrl ? var' is short form of 'ctrl.in  ? val'
'ctrl.in.any' and 'ctrl.out.any' have no short forms.
If target device have only one physical channel, then channels are multiplexed over physical channels.
(Daisy chaining of devices via ctrl channels might be possible).

Screen.any is always true.
Keyboard.any is true only when there is key pressed on development device's keyboard in keyboard buffer.
Ctrl.out.any on development device is ready when target device is connected and should always be ready to communicate.
Ctrl.in.any  on development device is always? ready after performing ctrl.out.

Development process by using development system
----------------------------------------------

User runs development system and he is presented with editor.

All commands in the editor are invoked with single keystroke (ctrl + key).

User develops program in assembly language.

There is command 'build' in this editor and it results in running the assembler on text from editor.
Assembler's activity can either result in
  - executable code for target system being produced or
  - error message displayed in editor with cursor positioned at error location.

There is 'current target' always selected in development system and it is always visually presented in editor.
Target can be emulator or device.
Current target can be cycled between emulator and target device (if target device is connected) using command 'target'.

There is 'run' command to run last built executable code.
Code can be run either in emulator or on target device depending on 'current target'.

When 'run' is executed new editor (run editor) is started.
Run editor can be in ctrl mode or connected mode (visually always presented on screen, similarly to 'current target').
Mode can be switched with 'target' command.

In ctrl mode this editor can:
  - reset remote system (move it to ready mode),
  - transfer executable code and run it on target (if it is in ready mode) or
  - stop it (move it to ready mode)
  - just run sent code on target (if in ready mode)
  - eventually peek and poke can be added to be used in ready mode
  - if program on target device is finished (executed stop instruction) device moves to ready mode automatically
  - if program ends with error, this error will be displayed similarly to error in assembly.
  - query status of connected system
  - execute editing commands on text from connected mode
  - quit to development's system editor

In connected mode keyboard and screen from development system are connected to in and out channels on current target. (to rethink: if edit mode is on, keyboard input is first treated as keyboard input for editor, and only afterwards sent to target's in channel, maybe it should be multiplexed with screen channel input)

Mode can be switched with 'target' command.

Run editor always starts by sending executable code to target and starting it and only afterwards switch to connected mode.

note: 'build' command might be removed as command and it's processes can be started always before 'run'.

there might also be 'swap' command to switch between those two editors.



----------------------------------------
Process of developing development system
----------------------------------------

Development system is developed in pascal programming language and compiled with free pascal compiler. It should be compilable on all popular platforms.

In the beginning, sublime-text editor will be used as development system editor (due to its availability on many platforms). As soon as possible, editor should be developed in pascal, to create tighter integration of development system components.

Prototype of development system will be done for Z80 target device, with modified assembler for Z80 and emulator as target.
It will emulate deki's breadboard Z80.


--------------------------------------
Unsorted thoughts - draft of the draft
--------------------------------------

- Every target system should implement channel communication.
- When target is connected to development system it should be clearly indicated in development system.
- one can send either byte or length::bytes on channel but what happens when sender stops ni the middle of sending array of bytes
- Channel status, time-outs and similar stuff should obviously be sorted out
- Target assembly should have stop ! ? and error instructions maybe via syscal.

channels and hardware
---------------------
- original and ds links of inmos should be thoroughly investigated
- maybe there is similar simple but efficient point to point differential bidirectional hardware and protocols
- RS232, USB via commodity hardware, maybe sort of ram/rom emulator or dma sram interface
- there should be some firmware on target hardware, but maybe intermediary hardware can be universal (arduino, pics or something, with channels multiplexed over USB)
- in that case only things needed are channel abstractions on devsys and in intermediary hardware's firmware


Multiplexing
------------
- simple multiplexing with striping protocol type
- multiplexing on channels via first byte/bit on channel (eg. 0 for control, 1 for in/out).

      receive:
        WHILE true
          SEQ
            mul ? type; msg
            CASE type OF
              ctrl_ch : ctrl ! msg
              io_ch   : out  ! msg

      send:
        WHILE true
          ALT
            ctrl ? msg
              mul ! ctrlch; msg
            in ? msg
              mul ! ctrlch; msg

control channel protocol
------------------------
- obviously there should be some sort of link implemented on target system
- could be usb -> commodity hardware -> link
- target system should support at least something of control protocol - reset, upload and run
- errors in software on target hardware

- target: ready | devsys: upload prog nr x | target: ready | devsys: run prog nr. x | target: running x; | target: done
- target: running | devsys: stop | target: ready
- devsys: id and status | target:
- target: error 'msg';ready
- devsys: reboot | target: ready
- devsys: stop | target: ready | devsys: peek and poke

- daisy chaining control channels through multiple devices?

target hardware facilities
--------------------------
- ctrl link (parallel in background, always ready to stop hardware, inaccessible to programs)
- in/out link (keyboard/screen)
- store multiple programs?
- run program
- stop instruction to end current program
- skip instruction (nop)
- input/output
- syscal instructions (in!x, out?x, stop, error)

- next level
  - channels
  - scheduling, communication
  - permanent storage
  - timers, signals
  - guarded instructions (alt)

- think about quemu and minos
