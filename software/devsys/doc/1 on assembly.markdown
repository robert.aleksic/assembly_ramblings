what is the problem we are trying to address
--------------------------------------------

Assembly programming has been always highly tied to unique combination of processor, accompanying hardware and operating system. This resulted in difficulties in code reusability. Situation is becoming even more complicated with diversification in all of those three areas.

One source of difficulty is differences in assembly mnemonics, assembly style of various vendors and available processor features (risc vs cisc, embedded vs. multi purpose, non orthogonality of addressing modes, different processor modes, bit width of registers and memory interface)

how are we trying to address it
--------------------------------

By defining unified assembly language and introducing higher level concepts, namely: procedures, functions, constants, variables, constant expressions and program flow constructs (if-then-else, repeat-until, while-do, case) while keeping one statement one instruction principle as much as possible.

We will start with 8-bit processors and z80 for the first case study and original mnemonics.

There will be lessons to be learned from this attempt and first we will try to cover variations of z80 family successors and predecessors (datapoint 8008, i8080, z80, i8086 to name a few).

about development system
------------------------

Despite having variety of options to use emulators, interpreters, editors, assemblers, cross assemblers etc, we think that it is a good idea to create unified development system since it seems that it is not to complicated thing to do unless we try to do full electronic level simulation.

In first iteration we will use sublime text as editor (it is simple and small comparing with for example eclipse and can be run on any platform) and create three utilities - assembly, emulate and run (to transfer and run code on destination platform). This approach should provide seamless experience for developer across platforms and target hardware. Features like separate compilation and linking will be avoided with language features and 'always compile everything' approach. Given current processor speeds and memories this should present no problem for development system.

Before this development system can be transferred on destination platform development system should be produced as one program including edit, assembly, emulate and run. This step will be postponed since before this step we need to unify input/output and some minimal common features (such as text mode, keyboard support etc) which are already available on development system's platform.

about unified assembly
----------------------

In first iteration mnemonics will be eventually slightly renamed, also source/destination, addressing modes, pre/post inc/dec addressing mode should be unified. Register naming and aliasing will be implemented to address variations in register names.

First idea is to name 8-bit registers r0..rn and 16-bit registers w0..wn and afterwards define which of them are composed of existing 8/bit registers. One of those mappings could be:

  a,f,b,c,d,e,h,l,i,r is r0..r9
  af,bc,de,hl,ix,iy,sp,pc is w0..w7

  w0,w1,w2,w3 remaps (r0,r1), (r2,r3), (r4,r5), (r6,r7)

This enable using standard z80 register naming and register names are simply newly introduced register names. Potential issues with this approach are related to polluting name space and local contexts.

Anyway, this means that example assembly from wikipedia:

     ; memcpy --
     ; Copy a block of memory from one location to another.
     ;
     ; Entry registers
     ;      BC - Number of bytes to copy
     ;      DE - Address of source data block
     ;      HL - Address of target data block
     ;
     ; Return registers
     ;      BC - Zero

                 org     1000h       ;Origin at 1000h
     memcpy      public
     loop        ld      a,b         ;Test BC,
                 or      c           ;If BC = 0,
                 ret     z           ;Return
                 ld      a,(de)      ;Load A from (DE)
                 ld      (hl),a      ;Store A into (HL)
                 inc     de          ;Increment DE
                 inc     hl          ;Increment HL
                 dec     bc          ;Decrement BC
                 jp      loop        ;Repeat the loop
                 end

can become something like:

    // returns zero in temp register and auto set z flag; maybe temp must be a/r0
    func zero.w (const w:word; using temp:byte) inline
      mov.b  temp, w.hi
      or.b   temp, w.lo

    proc memcopy.b (var count,source,destination:word; using temp:byte) // call or inline or ommited
      while:
        zero.w (count) using temp
        jp.zero whend: // can be optimized to ret depending on call context
        mov.b  temp, [source]
        mov.b  [destination], temp
        inc.w  source
        inc.w  destination
        dec.w  count
        jp     while
      whend: // ret or nothing depending on call context

and btememcopy can be called in two ways:

    inline memcopy.b (bc, de, hl) using a // with literal parameter replacing

or

    call memcopy.b (bc, de, hl) using a



or even

    proc memcopy.b (var count,source,destination:word; using temp:byte)

      func zero.w (const w:word; using temp:byte) inline
        mov.b  temp, w.hi
        or.b   temp, w.lo

      while not zero.w (count) using temp
        mov.b  temp, [source]
        mov.b  [destination], temp
        inc.w  source
        inc.w  destination
        dec.w  count

and

    memcopy        (bc,de,hl) using a // to optimise program memory usage
    memcopy.inline (bc,de,hl) using a // to optimise call time

Explicit naming of used registers is introduced in order to minimize side effects and enable implementation of various calling conventions. While loop in second example enable avoiding labels and explicit jumps.

This certainly need to be polished in many ways (while loop and nesting) but it clearly demonstrate potential of func and proc reusability (be it call or inline) while preserving (if not enhancing) code compactness and readability. It can also be made safer than macro concept in existing assemblers due to const/var/using explicit declarations.

Calling convention implementation is of course yet to be cleared somehow (both implicit or explicit).

hardware interface
------------------

Hardware connected to development system should have ability for two way communication. This can be achieved in various ways... elaborate here

Two features are must for high interactivity during development:
  - download and execute and
  - remote reset

We can say that hardware can be in two states: awaiting program and executing program. Remote reset always puts hardware in awaiting program mode (maybe with some signalling that it is ready to receive it). If simple, two way communication is implemented then some kind of remote terminal functionality can be implemented afterwards. Therefore minimum software on given hardware should include: reset, receive byte, send byte and program receive and execute.

baby steps
----------

1. create z80 interpreter (maybe selected instructions only)
2. create z80 assembler (minimum intervention - mnemonics, const, vars, aliases for registers)
3. tie evrything up in development system
4. play with it
5. in paralel develop hardware and iterate until it is clean
6. slowly introduce new concepts in assembly - constant expressions, if - then - else, while an get rid of jumps
7. play with it bouth with hardware and emulator
8. more concepts - functions and procedures, libraries (no linker or separate compilation)
9. try tight small programs like 1k compentitions and simmilar
10. iterate or give up
11. if not givenup up think of some other 8-bit or 16-bit processor
12. if given up document thoroughly
13. if not given up document thoroughly :)

- think about wider audience / people which want to take part in design
- think about it in terms of control network except it's only one system connected
- think about channels as in csp
