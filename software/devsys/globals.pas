unit globals;
interface

const
  codefile = 'a.out';

type
  word16 = word;

  // error = 0   -> ok
  // error = 255 -> not enough memory for error message
  // error = 254 -> shortened error message

  procedure terminate (code:byte; msg:string);

implementation
uses
  memory;

  procedure terminate (code:byte; msg:string);
  var
    l,i,free : memref;

  begin
    l := length (msg);
    free := hi-lo+1;

    mem [lo] := code;

    if free < l+2 // shorten message
    then if free=1 // space just for error code
         then mem[lo] := 255
         else begin
                mem[lo] := 254;
                l := free-2
              end;

    if mem[lo]<>255
    then begin
           mem[lo+1] := l;
           for i := 1 to l do
             mem[lo+1+i] := ord (msg[i])
         end
  end;

end.