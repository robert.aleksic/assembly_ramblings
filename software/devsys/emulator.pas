{$mode objfpc}
unit emulator;
interface
type
  proc_type = (z80,hex8);

  // mem [lo] = 0; mem [lo+1] = size; code = mem [lo+2..]
  procedure emulate (processor:proc_type);

implementation

uses
  globals, memory, channels;

type
  procstate = (running, stopped, extstopped, memtrap, illegal);


  procedure emulate_z80 (var mems; max:word16; var s:procstate);
  const maxmem = 64*kilo;
        stop = 0; skip = 1;

  type  memref = 0..maxmem-1;

  var
    mem   : array [memref] of byte absolute mems;
    ip    : word16;
    inst  : byte;

    procedure nextinst;
    begin
      inst := mem[ip];
      // check memory and external stop
      ip := ip+1
    end;

    procedure execute;
    begin
      case inst of
        skip : nextinst;
        stop : s := stopped
        else s := illegal
      end
    end;

  begin
    repeat
      if externalstop
      then s := extstopped
      else execute
    until s<>running;
  end;

  procedure emulate_hex8 (var mems; max:byte; var s:procstate);
  begin
  end;

  procedure emulate (processor:proc_type);
  var last, codesize : memref; s : procstate;
  begin
    s := running;
    // memory from lo: err=0, codelen.hi, codelen.lo, code
    if mem[lo]<>0
    then terminate (1,'no code to execute in emulator')
    else begin
           last     := hi-lo-3; // 0..last
           codesize := mem[lo+2]*256+mem[lo+1]; // two butes for now
           if codesize > last+1
           then terminate (2,'code size seems to be bigger then memory available')
           else begin
                  case processor of
                  z80 :  begin
                           if last>=64*kilo-1 then last := 64*kilo-1;
                           emulate_z80 (mem[lo+3],last, s)
                         end;
                  hex8 : begin
                           if last>=255 then last := 255;
                           emulate_hex8 (mem[lo+3],last, s);
                         end
                  end;
                  case s of
                    stopped    : terminate (0,'ok');
                    extstopped : terminate (1,'program stoped due to external stop');
                    memtrap    : terminate (2,'program tried to accsess memory out of it''s bounds');
                    illegal    : terminate (3,'program tried to execute illegal instrunction')
                  end
                end
         end
  end;

begin
end.