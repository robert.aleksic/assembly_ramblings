unit memory;
interface

const
  kilo = 1024;
  mega = kilo*kilo;
  giga = kilo*mega;

  memsize = 1*giga; // use 64-bit compiler if > 4GB

type
  memref = 0..memsize-1;

const
  ptrsize = sizeof (memref);

var
  mem   : packed array [memref] of byte;
  lo,hi : memref;

  procedure init;

implementation

  procedure init;
  begin
    lo := low (memref); hi := high (memref)
  end;

begin
end.