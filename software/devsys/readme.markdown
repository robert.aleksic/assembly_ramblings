==================
Development system
==================

const
  memsize = ...

type
  memref = 0..memsize-1;

var
   mem   : array [memref] of byte; {packed?}
   lo,hi : memref; // free mem for subprocesses is mem[lo..hi]

   retlo, rethi : memref; // result of run in mem[lo..reslo], mem[reshi..hi]

begin
  if statesaved
  then restorestate
  else begin
         lo := 0; mem := max(memref);
         initeditorstate; // bgcolor, cx, cy, commkeys, exitcmds, exitcmd, textstart := lo
       end
  done := false;
  repeat
    editor_process_cal;
    case exitcmd of
      assembly : assembler_process_cal;
      run      : emulator_process_call;
      devquit  : done := true
      else beep
    end
  until done;
  savestate // lo,hi,mem[0..lo),mem(hi..hi], editorstate
end.

Memory (statically allocated array of bytes)
--------------------------------------------
  - might move to 64 bits and dynamic allocation
    - mem should then be referenced as mem^

Calling processes
-----------------
  - save lo and hi
  - run process (editor, assebler, run)
  - save result if needed (savedlo..lo-1,hi+1..savedhi)
  - restore lo and hi

Editor (non modal)
-------------------
- top line for curx, curry and status
- bottom line for messages
- will use crt unit for keyboard and screen (will be replaced with ncourses alternative for ssh functionality)
- can be run in window or full screen with auto redraw on resize (problematic)
- full screen would be best on separate desktop in modern os's
- would be nice to support utf-8 somehow seems that utf-32 is best for representation
- ctrl/option used as modifiers
- highliting reserved words would be nice (maybe with underline)
- folding editor if we know how to save state

Assembler
---------
- return compiled code or message and position (0,codelen,code or err,message in savedlo..lo-1) (savedlo must be > lo)
- min codelen = 1 -> stop

z80
---
- stop = HALT
- skip = NOP or nothing

  const
    out.chan = 0
    in.chan  = 1
    empty    = 0

- out ! A <=> OUT out.chan, A
- in.any  <=> LD A, (in.chan+1)
- in  ? A <=> repeat in.any until A=empty; LD A, (in.chan); LD (i.chan+1), empty

- in channel handled via interrupt, sets i.chan to key, sets i.chan+1 memory to not empty

- outside hardware can buffer keyboard or use ports for communication
- ctrl channels handled in outside hardware for now
