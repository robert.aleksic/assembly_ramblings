{$mode objfpc} {$longstrings on}
program keys;
uses
  keyboard, screen;

var
  ch : string;
  me : mouseevent;

begin
  init;

  repeat

    waitch (ch,me);

    case speckey (ch) of
      knone  : if (length(ch)>1) or ((ord(ch[1])>=32) and (ord(ch[1])<128))
               then writeln (ch,' code: ',ord(ch[1]))
               else writeln ('code: ',ord(ch[1]));
      kother : writeln ('other: esc+', copy (ch,3,length(ch)-2));
      kmouse : begin
                 write ('mouse: ');
                 with me do
                 begin
                   write (but,' ',cas, ' ',x,' ',y,' ');
                   if up
                   then writeln ('up')
                   else writeln ('down')
                 end
               end;
      kesc   : writeln ('esc');
      ktab   : writeln ('tab');
      kent   : writeln ('enter');
      kbksp  : writeln ('backspace');
      kup    : writeln ('up');
      kdown  : writeln ('down');
      kleft  : writeln ('left');
      kright : writeln ('right');
      else writeln ('spec: ',ord(ch[2]))
    end;

  until ch = 'q';

  done
end.