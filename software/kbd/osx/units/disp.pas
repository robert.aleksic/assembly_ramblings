{
  Character is two byte utf-16 single codepoint character (multiword code points are not supported).
  Color is one of 0..maxcolors where maxcolors is 255 (in accordance with 256 color palete of xterm-256-colors).

  Screen is matrix of characters with idividual background and foreground color of maximum size maxrows by maxcols.

  Display is window into screen which have dimensions rows x cols, originating in upper left corner of the screen.
  Size of display can vary during the time of program execution, independently of program code.

  This unit enables manipulation of screen's characters and their visual representation on display.
  It keeps tab on display content to accomodate quick updates when content of screen changes.
  Disiplay resizes are handled automaticaly by using main event loop (wait) to redraw screen on changes.

  Colors, keyboard, mouse...
}

{$mode objfpc} {$longstrings on}
unit disp;

interface

uses
  base;

const
  // imac 27", 10pt monaco, fullscreen (425 x 102)
  // imac 27", 14pt monaco, fullscreen (318 x 75)
  // imac 27", 18pt monaco, fullscreen (231 x 57)
  maxcols = 512;
  maxrows = 128;

type
  color = record
            case boolean of
              true  : (bg, fg : word8);
              false : (bgfg   : word16)
          end;
  cell = record
           c  : color;
           ch : widechar
         end;

  linebuff  = array [0..maxcols] of cell;
  scrbuff   = array [0..maxrows] of linebuff;

  screnum = (screen, display);

var
  buff : array [screnum] of scrbuff;

  cols, rows  : integer;
  curx, cury  : integer;
  curcol      : color;

  procedure init (fg:integer = -1; bg:integer = -1);
  procedure done;
  procedure clear;
  procedure update;

  procedure setyx  (col,row : integer);
  procedure setcol (fg:integer; bg:integer=-1);

  procedure justout (w:widechar);
  procedure paint (fg,bg:integer; len:integer=-1; y:integer=-1; x:integer=-1);
  procedure out (s : string; len:integer=0);
  procedure out (i : int64;  len:integer=0);
  procedure box (x,y,w,h:integer; clear:boolean=false);

  procedure getbox (var b:array of cell; x,y,w,h:integer);
  procedure putbox (var b:array of cell; x,y,w,h:integer);

  procedure wait (var ch:string);
  procedure wait;



implementation

uses
  sysutils, term, keyboard;

var
  lcol       : color; // last painted color
  nocoloryet : boolean;

  procedure init (fg:integer=-1; bg:integer=-1);
  var
    y : integer;
  begin
    term.init;
    rows := term.rows;
    cols := term.cols;
    keyboard.init;

    nocoloryet := true;
    if fg=-1 then fg := 15;
    if bg=-1 then bg := 0;
    setcol (fg,bg);

    clear;
    for y := 1 to maxrows do
      buff[screen][y,0].ch := ' ';
    buff [display] := buff[screen];
    term.clear // initial redraw
  end;

  procedure done;
  begin
    keyboard.done;
    term.done
  end;

  procedure clear;
  var x,y:integer;
  begin
    buff[screen,1,0].ch := '*';
    for x := 1 to maxcols do
      with buff[screen][1,x] do
      begin
        ch := ' ';
        c  := curcol
      end;
    for y := 1 to maxrows do
      buff[screen][y] := buff[screen][y-1]
  end;

  procedure setyx (col,row:integer);
  begin
    if cury <> -1 then cury := col;
    if curx <> -1 then curx := row
  end;

  procedure setcol (fg:integer; bg:integer=-1);
  begin
    if fg <> -1 then curcol.fg := fg;
    if bg <> -1 then curcol.bg := bg
  end;



  procedure markforupdate (l:integer);
  begin
    buff[screen][l,0].ch := '*'
  end;

  procedure paint (fg,bg:integer; len:integer=-1; y:integer=-1; x:integer=-1);
  var i : integer;
  begin
    if len = -1 then len := 1; if y = -1 then y := cury; if x = -1 then x := curx;
    if fg <> -1 then for i := x to x+len-1 do buff[screen,y,i].c.fg := fg;
    if bg <> -1 then for i := x to x+len-1 do buff[screen,y,i].c.bg := bg;
    markforupdate (y)
  end;

  procedure box (x,y,w,h:integer; clear:boolean=false);
  const vert = '│'; horiz = '─'; ul = '┌'; ur = '┐'; ll = '└'; lr = '┘';
  var l : string; i : integer;
  begin
    l := ''; for i := 1 to w-2 do l := l+horiz;
    setyx (y,x);     out (ul+l+ur);
    setyx (y+h-1,x); out (ll+l+lr);
    if not clear
    then for i := 1 to h-2 do
         begin
           setyx (y+i,x);     out (vert);
           setyx (y+i,x+w-1); out (vert);
         end
    else begin
           l := ''; for i := 1 to w-2 do l := l+' ';
           l := vert+l+vert;
           for i := 1 to h-2 do
           begin
             setyx (y+i,x);
             out (l)
           end
         end;
    setyx (y+1,x+1);
  end;

  procedure justout (w:widechar);
  begin
    with buff[screen,cury,curx] do
    begin
      ch := w;
      c  := curcol;
      markforupdate (cury)
    end;
    if curx<maxcols then curx := curx+1
  end;

  procedure out (s : string; len:integer=0);
  var pos,l,pad,i : integer;
  begin
    l := length(s);
    if len<>0
    then begin
           pad := abs(len)-l;
           l   := abs(len);
           if pad<0
           then begin
                  s := '';
                  for i := 1 to abs(len) do s := s+'*'
                end
           else if len<0
                then for i := 1 to pad do s := s+' '
                else for i := 1 to pad do s := ' '+s
         end;
    pos := 1;
    while (curx>=1) and (curx<=maxcols) and
          (cury>=1) and (cury<=maxrows) and
          (pos<=l) do
      justout (utf16fromutf8 (s,pos))
  end;

  procedure out (i:int64; len:integer=0);
  var s:string;
  begin
    str (i,s);
    out (s,len)
  end;



  procedure updateline (y:integer; redraw:boolean);
  var f,l : integer;

    procedure doupdate (y,f,l:integer);
    var
      s      : string;
      i,j,ls : integer;
      graph  : boolean;

      function ch (i:integer) : widechar;
      begin ch := buff[screen,y,i].ch end;

      function graphind (i:integer) : integer;
      var w : word16;
      begin
        w := word16(buff[screen,y,i].ch);
        if (w<$2500) or (w>$253c)
        then graphind := -1
        else case w of
               $2500 : graphind :=  1;
               $2502 : graphind :=  2;
               $250c : graphind :=  3;
               $2510 : graphind :=  4;
               $2514 : graphind :=  5;
               $2518 : graphind :=  6;
               $251c : graphind :=  7;
               $2524 : graphind :=  8;
               $252c : graphind :=  9;
               $2534 : graphind := 10;
               $253c : graphind := 11;
               else    graphind := -1
             end
      end;

      function isgraph (i:integer) : boolean;
      begin
        isgraph := graphind(i) <> -1
      end;

      function graphch (i:integer) : char;
      var chars : array [1..11]  of char = 'qxlkmjtuwvn';
      begin
        graphch := widechar(ord(chars [graphind(i)]))
      end;

      procedure termsetcolor (c:color);
      begin
        if nocoloryet
        then begin term.setfgbg (c.fg,c.bg); nocoloryet := false end
        else if lcol.bgfg<>c.bgfg
             then if lcol.bg=c.bg
                  then term.setfg (c.fg)
                  else if lcol.fg=c.fg
                       then term.setbg (c.bg)
                       else term.setfgbg (c.fg,c.bg);
        lcol := c
      end;

    begin
      termsetcolor (buff[screen,y,f].c);
      term.move (y,f);

      i := f;
      while i<=l do
      begin
        graph := isgraph (i);

        ls := i+1;
        if graph
        then begin while (ls<l) and     isgraph (ls) do ls := ls+1; if not isgraph (ls) then ls := ls-1 end
        else begin while (ls<l) and not isgraph (ls) do ls := ls+1; if     isgraph (ls) then ls := ls-1 end;

        s := '';
        for j := i to ls do
        begin
          buff[display,y,j] := buff[screen,y,j];
          if graph then s := s + graphch (j) else s := s + utf8fromutf16 (ch(j))
        end;

        if graph then term.charset (false);
        write (s);
        if graph then term.charset (true);

        i := ls+1
      end;

      if redraw and (l=cols) then write (' ')  // patch for remains when resizing on os x
    end;

    function lastsamecolfrom (i:integer) : integer;
    var
      col  : color;
      last : integer;
    begin
      col := buff[screen,y,i].c;
      last := i+1;
      while (last <= cols) and (col.bgfg=buff[screen,y,last].c.bgfg) do
        last := last+1;
      last := last-1;
      lastsamecolfrom := last
    end;

    procedure process (f,l:integer);
    var s,e : integer;

      function diff (i:integer) : boolean;
      begin
        with buff[screen,y,i] do
          diff := ch<>buff[display,y,i].ch
      end;

    begin
      s := f;
      while s<=l do
      begin
        while (s<l) and not diff (s) do s := s+1;
        if diff(s) // first after or equal to s which is diff
        then begin
               e := s;
               while (e<l) and diff(e) do e := e+1;
               if not diff(e) then e := e-1;
               doupdate (y,s,e);
               s := e + 1
             end;
      end
    end;

  begin
    f := 1;
    while f<=cols do
    begin
      l := lastsamecolfrom (f);
      if redraw
      then doupdate (y,f,l)
      else process (f,l);
      f := l+1
    end;
    buff[screen,y,0].ch := ' '
  end;

  procedure update;
  var y : integer;
  begin
    for y := 1 to rows do
      if buff[screen,y,0].ch<>' '
      then updateline (y,true)
  end;

  procedure redraw;
  var y : integer;
  begin
    for y := 1 to rows do
      updateline (y,true)
  end;

  procedure getbox (var b:array of cell; x,y,w,h:integer);
  var
    i,j,p : integer;
  begin
    if length(b)<w*h
    then begin
           disp.done;
           writeln ('buffer for getbox to short');
           halt(1)
         end;

    p := 0;
    for i := y to y+h-1 do
      for j := x to x+w-1 do
      begin
        b[p] := buff[screen,i,j];
        p := p+1
      end
  end;

  procedure putbox (var b:array of cell; x,y,w,h:integer);
  var
    i,j,p : integer;
  begin
    if length(b)<w*h
    then begin
           disp.done;
           writeln ('buffer for putbox to short');
           halt(1)
         end;

    p := 0;
    for i := y to y+h-1 do
    begin
      for j := x to x+w-1 do
      begin
        buff[screen,i,j] := b[p];
        p := p+1
      end;
      markforupdate (i)
    end
  end;


  procedure wait (var ch:string);
  //var k  : tkeyevent;
  begin
    while not keypressed or term.resized do
    begin
      sleep (10);
      if term.resized
      then begin
             term.resized := false;
             term.getscreensize;
             cols := term.cols;
             rows := term.rows;
             redraw
          end
    end;
    waitch (ch)
  end;

  procedure wait;
  var ch:string;
  begin
    wait (ch)
  end;

begin
end.