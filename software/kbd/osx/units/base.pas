{$mode objfpc} {$longstrings on}
unit base;

interface

type
  int8  = shortint;
  int16 = longint;
  int32 = smallint; // default integer in freepascal
  // int64 is already defined in freepascal

  word8  = byte;
  word16 = word; // default word in freepascal
  word32 = longword;
  word64 = qword;

  systemtype = record
             ostype : (unkn,win,osx,linux);
             linuxconsole : boolean
            end;

var
  csys : systemtype;



implementation



begin
  with csys do
  begin
    ostype := unkn;
    linuxconsole := false;
    {$IFDEF linux}   ostype := linux {$ENDIF}
    {$IFDEF windows} ostype := win   {$ENDIF}
    {$IFDEF darwin}  ostype := osx   {$ENDIF}
  end
end.