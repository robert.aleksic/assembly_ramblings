{$mode objfpc} {$longstrings on}
// Basic latin               041-05a (+20)
// Latin-1                   0c0-0d6, 0d8-0de (+20)
// Latin extended-A          100-137, 139-17f
// Latin extended-B          182-199, 19d-1a8, 1ac-1bd, 1cd-1dc, 1de-1ef, 1f4-1f5, 1fa-21f,
//                           222-233, 23b-23c, 241-242, 248-24f;
//                           181 (-1); digraphs 01c4-01c9, 1f1-1f3 (u,c,l) Dž Lj Nj Dz;

// Grrek and coptic          3e2-3ef; 391-3a1, 3a3-3ab (+20)
// Cyrillic                  460-481, 48a-4ff; 410-42f (+20); 400-40f (+50)
// Cyrillic supplement       500-52f
// Armenian                  531-556 (+30)
// Georgian                  10a0-10c5 (+30)
// Latin extended additional 1e00-1e95,1ea0-1eff
// Greeek extended           1f08-1f0f, 1f18-1f1d, 1f28-1f2f, 1f38-1f3f, 1f48-1f4d, 1f59, 1f5b, 1f5d, 1f5f, 1f68-1f6f (-8)
// Glagolitic                2c00-2c2e (+30)
// Latin extended-C          2c60-2c61, 2c67-2c6c, 2c72-2c73
// Cyrillic extended-B       a640-a66c, a680-a69b
// Latin extended-D          a722-a76f, a779-a787, a78b-a78e, a790-a7ad
// halfw and fullw forms     ff21-ff3a (+20)

unit utf;
interface
uses
  base;

type
  utfchar    = word32;
  utf8string = string;
  utf8char   = string;

  alignment = (left,center,right);



  function utf8len (s:utf8string) : integer;
  function utf8ch  (s:utf8string; i:integer) : utf8char;
  function utf8pos (s:utf8string; n:integer) : integer;

  function utf8ins   (s:utf8string; p:integer; sub:utf8string) : utf8string;
  function utf8del   (s:utf8string; p:integer; n:integer =  1) : utf8string;
  function utf8subst (s:utf8string; p:integer; n:integer = -1) : utf8string;

  function utffromutf8 (ch:utf8char) : utfchar;
  function utf8fromutf (w:utfchar)   : utf8char;

  function utf8chatpos (s:utf8string; var p:integer) : utf8char;
  //
  // utf-8 char from utf-8 string with p increment for iteration
  // usage examples:
  //
  //   pos := 1;
  //   repeat
  //     ch := utf8chatpos (s,pos);
  //     if ch <> '' then process ch
  //   until ch = ''
  //
  //   pos := 1;
  //   ch := utf8chatpos (s,pos);
  //   while ch<>'' do
  //   begin
  //     process ch;
  //     ch := utf8chatpos (s,pos)
  //   end
  //

  function utf8fromutf16 (wc:widechar) : string;
  function utf16fromutf8 (s:string; var pos:integer) : widechar;

  function utf8formatted (s:string; w:integer; al:alignment=left) : string;


implementation

const
  bit0 = 1;  bit1 = 2;  bit2 =  4; bit3 = 8;
  bit4 = 16; bit5 = 32; bit6 = 64; bit7 = 128;

  ones0 = bit0-1; ones1 = bit1-1; ones2 = bit2-1; ones3 = bit3-1;
  ones4 = bit4-1; ones5 = bit5-1; ones6 = bit6-1; ones7 = bit7-1; ones8 = 255;

  ones : array [0..8] of byte = (ones0, ones1, ones2, ones3, ones4, ones5, ones6, ones7, ones8);


  {  lowercase
           #$41..#$5a : buff[le] := chr (ord(ch)+32); // 'A'..'Z'
           #$c0..#$df : begin // two byte unicode
                          b1 := ord(ch);
                          b2 := ord(buff[le+1]);
                          if (b2 and $c0) = $80 // proper second byte
                          then begin
                                 c1 := (b1 shr 2) and 7;
                                 c2 := (b2 and 63) or ((b1 mod 4) shl 6);

                                 case c1 of
                                   00 : case c2 of // latin supplement
                                          $c0..$de : if c2<>$d7 then c2 := c2+32;
                                          else
                                        end;
                                   01 : case c2 of // latin extended
                                          $00..$7d  : if ((c2 < $78) and not odd(c2)) or ((c2 > $78) and odd(c2)) then c2 := c2+1;
                                          $cd..$de  : if not odd(c2) then c2 := c2+1;
                                          else
                                        end;
                                   03 : case c2 of // greek
                                          $91..$ab  : c2 := c2+32;
                                          else
                                        end;
                                   04 : case c2 of // cyrilic
                                          $00..$0f  : c2 := c2+80;
                                          $10..$2f  : c2 := c2+32;
                                          $60..$81,
                                          $8a..$ff  : if not odd(c2) then c2 := c2+1;
                                          else
                                        end;
                                   05 : case c2 of // cyrilic suplement
                                          $00..$2f  : if not odd(c2) then c2 := c2+1;
                                          else
                                        end;
                                   else

                                 b1 := ((c1 and 7) shl 2) or (c2 shr 6) or $c0;
                                 b2 := (c2 and 63) or $80
                               end;
                          buff[le]   := chr(b1);
                          buff[le+1] := chr(b2);
                          le := le+1
                        end
}


  function utf8extbyte (b:byte) : boolean; inline;
  begin
    utf8extbyte := (b and byte (bit7 + bit6)) = bit7
  end;

  function utf8extbyte (ch:char) : boolean; inline;
  begin
    utf8extbyte := utf8extbyte (ord(ch))
  end;

  function utf8extval (b:byte) : byte; inline;
  begin
    utf8extval := b and byte(ones6)
  end;

  function leadingones (b:byte) : integer;
  var i : integer;
  begin
    i := 0;
    while (b and byte(bit7)) <> 0 do
    begin
      i := i + 1;
      b := b shl 1
    end;
    leadingones := i
  end;

  function utf8len (s:utf8string) : integer;
  var i,l,e : integer;
  begin
    l := length (s); e := 0;
    for i := 1 to l do
      if utf8extbyte (s[i])
      then e := e+1;
    utf8len := l-e
  end;

  function utf8chatpos (s:utf8string; var p:integer) : utf8char;
  var ch:utf8char; len:integer;
  begin
    len := length(s);
    ch  := '';
    if p<=len
    then begin
           ch := s[p];
           p  := p+1;
           if (ord(s[p-1]) and byte(bit7)) <> 0
           then while (p<=len) and utf8extbyte(s[p]) do
                begin
                  ch := ch+s[p];
                  p  := p+1
                end
         end;
    utf8chatpos := ch
  end;

  function utf8pos (s:utf8string; n:integer) : integer;
  var l,i,p : integer;
  begin
    l := length(s); i := 1; p := 1;
    while (i<=l) and (p<n) do
    begin
      i := i+1;
      if not utf8extbyte (s[i]) then p := p+1;
    end;
    if (i<=l) and utf8extbyte (s[i])
    then i := i+1;
    utf8pos := i
  end;

  function utf8ch (s:utf8string; i:integer) : utf8char;
  var p:integer;
  begin
    p := utf8pos (s,i);
    utf8ch := utf8chatpos (s,p)
  end;

  function utf8ins (s:utf8string; p:integer; sub:utf8string) : utf8string;
  var
    i,pos,len : integer;
  begin
    len := utf8len (s);
    for i := len+1 to p do s := s+' ';
    pos := utf8pos (s,p);
    utf8ins := copy (s,1,pos-1) + sub + copy (s,pos,length(s)-pos+1)
  end;

  function utf8del (s:utf8string; p:integer; n:integer=1) : utf8string;
  var
    pos1,pos2 : integer;
  begin
    pos1 := utf8pos (s,p);
    pos2 := utf8pos (s,p+n);
    utf8del := copy (s,1,pos1-1) + copy (s,pos2,length(s)-pos2+1)
  end;

  function utf8subst (s:utf8string; p:integer; n:integer=-1) : utf8string;
  var
    pos1,pos2 : integer;
  begin
    pos1 := utf8pos (s,p);
    if n<0
    then utf8subst := copy (s,pos1,length(s)-pos1+1)
    else begin
           pos2 := utf8pos (s,p+n) - 1;
           utf8subst := copy (s,pos1,pos2-pos1+1)
         end
  end;

  function utffromutf8 (ch:utf8char) : utfchar;
  var
    w:word32; i,l:byte;

    procedure addbits (b,n:byte);
    begin
      w := (w shl n) or (b and ones[n])
      //;write (b,':',n,'=',w,'; ')
    end;

  begin
    w := 0;
    l := length(ch);
    if l=1
    then addbits (ord(ch[1]), 7)
    else for i := 1 to l do
           if i=1
           then addbits (ord(ch[1]), 7-l)
           else addbits (ord(ch[i]), 6);
    utffromutf8 := w
  end;

  function utf8fromutf (w:utfchar)  : utf8char;
  var ch:utf8char; nextw:utfchar; b,c:byte;
    procedure add (b:byte);
    begin
      //write (b,' ');
      ch := chr (b) + ch
    end;
  begin
    ch := '';
    if w<bit7
    then add (w)
    else begin
           c := 1;
           nextw := w shr 6;
           while nextw <> 0 do
           begin
             b := w and ones6;
             w := nextw;
             nextw := w shr 6;
             add (b or bit7);
             c := c+1
             //;write ('!',w,':',b,'!');
           end;
           add ((ones[c] shl (8-c)) + w)
         end;
    utf8fromutf := ch
  end;



  function utf8fromutf16 (wc:widechar) : string;
  var s:string; cp:word16;
    procedure addbyte (b:byte); begin s := s+chr(b) end;
  begin
    s := '';
    cp := word16(wc);
    if (cp and $7f) = cp
    then addbyte (cp)
    else if (cp and $7ff) = cp
         then begin
                addbyte (((cp shr 6) and $1f) or $c0);
                addbyte ((cp and $3f) or $80)
              end
         else begin
                addbyte (((cp shr 12) and $0f) or $e0);
                addbyte (((cp shr 6) and $3f) or $80);
                addbyte ((cp and $3f) or $80)
              end;
    utf8fromutf16 := s
  end;

  //
  // widechar from utf-8 string with pos increment for iteration
  // pos must be in 1..lenght(s)
  // usage:
  //
  //   pos := 1;
  //   while pos<=length(s) do
  //   begin
  //     ch := utf16fromutf8 (s,pos)
  //     process ch
  //   end
  //
  function utf16fromutf8 (s:string; var pos:integer) : widechar;
  var cp:word16; w,c,w1,w2,w3 : word8;
    function this:byte; inline; begin this := ord(s[pos]); pos := pos+1 end;
  begin
    w1 := this;
    if w1 < $80
    then cp := w1
    else begin
           w := w1; c := 0; while w>=$80 do begin w := w shl 1; c := c+1 end;
           if (c=1) or (c>3)
           then cp := ord ('?')
           else case c of
                  2 : begin
                        w2 := this;
                        cp := (w1 and $1f) shr 2;
                        cp := cp shl 8;
                        cp := cp or ((w1 and $03) shl 6) or (w2 and $3f)
                      end;
                  3 : begin
                        w2 := this;
                        w3 := this;
                        cp := (w1 and $f) shl 4;
                        cp := cp or ((w2 and $3f) shr 2);
                        cp := cp shl 8;
                        cp := cp or ((w2 and $03) shl 6) or (w3 and $3f)
                      end
                end
         end;
    utf16fromutf8 := widechar(cp)
  end;

  function utf8copy (s:string; f,len:integer) : string;
  var
    i,l,pos,num : integer; ss:string; wc:widechar;
  begin
    l := length(s);
    ss := ''; num := 0;
    i := 1; pos := 1;
    while (pos<=l) and (num<=len) do
    begin
      wc := utf16fromutf8 (s,pos);
      if (i>=f) and (num<=len)
      then begin num := num+1; ss := ss+utf8fromutf16 (wc) end;
      i := i+1;
    end;
    utf8copy := ss
  end;


  function utf8formatted (s:string; w:integer; al:alignment=left) : string;
  var i,p,l, lp,rp : integer;
  begin
    l := utf8len(s);
    if l>w
    then begin
           p := l-w;
           case al of
             left   : lp := 0;
             right  : lp := p;
             center : lp := p div 2
           end;
           s := utf8copy (s,lp+1,w)
         end
    else begin
           lp := 0; rp := 0;
           case al of
             left   : rp := w-l;
             right  : lp := w-l;
             center : begin
                        p := w-l;
                        lp := p div 2;
                        rp := p-lp
                      end
           end;
           for i := 1 to lp do s := ' '+s;
           for i := 1 to rp do s := s+' '
         end;
    utf8formatted := s
  end;

end.