// works on mac and linux, to do windows
// to do linux scancodes for better flexibility (ctrl-m enter, ctrl-i tab etc)


{$mode objfpc} {$longstrings on}
unit keyboard;
interface

type
  mouseevent = record
                 valid : boolean;
                 but   : 1..4;
                 cas   : 0..7;
                 x,y   : integer;
                 up    : boolean
               end;

  speckeys = (knone, kinvutf8, kmouse,
              kleft, kright, kup, kdown,
              khome, kend, kdel, kpgup, kpgdn,
              kf1, kf2, kf3, kf4,
              kf5, kf6, kf7, kf8, kf9, kf10,
              kbksp, kesc, kent, ktab, kother);

  procedure kbinit;
  procedure kbdone;

  function keypressed : boolean;
  function speckey (ch : string) : speckeys;

  procedure waitch (var ch : string);
  procedure waitch (var ch : string; var me : mouseevent);

implementation

uses
  sysutils, baseunix, termio;

{$ifndef _POSIX_VDISABLE}
const
  _POSIX_VDISABLE = $ff;
  VDSUSP=11;
{$endif}

var
  oldtio : termios;

  procedure setrawmode (b:boolean);
  var Tio : Termios;
  begin
    if not b
    then TCSetAttr (0,TCSANOW,oldtio)
    else begin
           TCGetAttr (0,tio);
           oldtio := tio;
           tio.c_iflag := tio.c_iflag and not (IGNBRK or BRKINT or PARMRK or ISTRIP or
                                               INLCR or IGNCR or ICRNL or IXON);
           //tio.c_oflag := tio.c_oflag and not OPOST;
           tio.c_lflag := tio.c_lflag and not (ECHO or ECHONL or ICANON or ISIG or IEXTEN);
           tio.c_cflag := tio.c_cflag and not (CSIZE or PARENB) or CS8;

           tio.c_cc [VMIN]  := 1;
           tio.c_cc [VTIME] := 0;

           tio.c_lflag := tio.c_lflag and not ISIG;
           tio.c_cc [VQUIT]  := _POSIX_VDISABLE;
           tio.c_cc [VSUSP]  := _POSIX_VDISABLE;
           tio.c_cc [VDSUSP] := _POSIX_VDISABLE;

           TCSetAttr (0,TCSANOW,tio)
         end
  end;

  procedure kbinit;
  begin
    setrawmode (true)
  end;

  procedure kbdone;
  begin
    setrawmode (false)
  end;


const
  bufflen  = 1024;
  speccode = #$ff;

  esc = #27;

type
  r = record s:string; sk:speckeys end;

const
  enc : array [1..20] of r = (
    (s:'OD'; sk : kleft),
    (s:'OC'; sk : kright),
    (s:'OA'; sk : kup),
    (s:'OB'; sk : kdown),
    (s:'OH'; sk : khome),
    (s:'OF'; sk : kend),
    (s:'OP'; sk : kf1),
    (s:'OQ'; sk : kf2),
    (s:'OR'; sk : kf3),
    (s:'OS'; sk : kf4),
    (s:'OM'; sk : kent),

    (s:'[3~'; sk : kdel),
    (s:'[5~'; sk : kpgup),
    (s:'[6~'; sk : kpgdn),

    (s:'[15~'; sk : kf5),
    (s:'[17~'; sk : kf6),
    (s:'[18~'; sk : kf7),
    (s:'[19~'; sk : kf8),
    (s:'[20~'; sk : kf9),
    (s:'[21~'; sk : kf10));

var
  buff : array [1..bufflen] of char;
  buffhead, bufftail : integer;


  function mouseev (s:string) : mouseevent;
  var
    m        : mouseevent;
    p,l      : integer;
    b1,b2,b3 : integer;

    function supto (ch:char) : integer;
    var ss : string; i,e : integer;
    begin
      ss := '';
      while (lowercase(s[p])<>ch) and (p<=l) do
      begin
        ss := ss + s[p];
        p := p+1
      end;
      val (ss,i,e);
      if (lowercase(s[p])<>ch) or (i<0) or (e<>0)
      then i := -1;
      supto := i
    end;

    function bit (i:integer) : boolean;
    begin
      bit := odd ((b1 shr (i-1)))
    end;

  begin

    p := 3; l := length(s);
    b1 := supto(';'); p := p+1;
    b2 := supto(';'); p := p+1;
    b3 := supto('m');

    with m do
    begin
      valid := (b1<>-1) and (b2<>-1) and (b3<>-1);
      if valid
      then begin
             x  := b2;
             y  := b3;
             up := s[p]='m';

             cas := (b1 div 4) mod 8;
             but := ord(bit(1));
             if bit(2)
             then but := but + 2;
             if bit(7)
             then but := but + 3
           end
    end;
    mouseev := m

  end;



  procedure match (escs:string; var spk:speckeys; var me:mouseevent);
  var i:integer;
  begin
    if escs=''
    then spk := kesc
    else if copy (escs,1,2) = '[<'
         then begin
                me := mouseev (escs);
                if me.valid
                then spk := kmouse
                else spk := kother
              end
         else begin
                spk := kother;
                for i := 1 to length (enc) do
                  with enc[i] do
                    if escs = s
                    then spk := sk
              end
  end;

  function specch (sk:speckeys) : string;
  begin
    specch := speccode + chr(ord(sk))
  end;

  function speckey (ch:string) : speckeys;
  begin
    if (length(ch)=1) or (ch[1]<>speccode)
    then speckey := knone
    else speckey := speckeys(ord(ch[2]))
  end;

  function have (n:integer) : boolean;
  begin
    have := n <= (bufftail-buffhead+1)
  end;

  function keypressed : boolean;
  begin
    if have (1)
    then keypressed := true
    else begin
           fpioctl (stdinputhandle,FIONREAD,@bufftail);
           keypressed := bufftail <> 0
         end
  end;

  procedure waitch (var ch : string; var me:mouseevent);

    procedure consume (n:integer);
    begin
      buffhead := buffhead + n
    end;

    function utf8ch : string;
    var b,l,i : integer;
    begin
      b := ord (buff[buffhead]);
      if b < 128
      then l := 1
      else if b div 32 = 6
           then l := 2
           else if b div 16 = 14
                then l := 3
                else if b div 8 = 30
                     then l := 4
                     else l := 1;
      if not have (l) // mising utf8 bytes from buffer
      then begin
             utf8ch := specch (kinvutf8);
             consume (bufftail-buffhead+1)
           end
      else begin
             b := -1;
             for i := l downto 2 do
               if ord(buff[buffhead+i-1]) div 64 <> 2
               then b := i;

             if b <> -1 // invalid utf8 byte
             then begin
                   utf8ch := specch (kinvutf8);
                   consume (b-1)
                  end
             else begin // utf8 ok
                    utf8ch := '';
                    for i := 0 to l-1 do
                      utf8ch := utf8ch + buff[buffhead+i];
                    consume (l)
                  end
           end
    end;

    function escseq : string;
    const termch = ['~','^','$','@','A'..'Z','a'..'z'];
    var i,p : integer;
    begin
      escseq := '';

      p := buffhead+1;
      if buff[p] in ['b','f']
      then escseq := buff[p]
      else if (buff[p]='O') or (buff[p]='[')
           then begin
                  p := p+1;
                  while not (buff[p] in termch) and (p<bufftail) do p := p+1;
                  if buff[p] in termch
                  then for i := buffhead+1 to p do escseq += buff[i]
                end
    end;

  var
    spk : speckeys;

    procedure outbuff;
    var i : integer;
    begin
      write ('head: ',buffhead,' tail: ',bufftail,' buff:');
      for i := buffhead to bufftail do write (' ',ord(buff[i]));
      writeln
    end;

  begin
    if not have (1)
    then begin
           while not keypressed do
             sleep (10);
           fpread (stdinputhandle,@buff,bufftail);
           buffhead := 1
           // ;outbuff
         end;

    if buff[buffhead] <> esc
    then ch := utf8ch
    else if buffhead=bufftail
         then begin ch := esc; consume(1) end
         else begin
                match (escseq, spk, me);
                ch := specch (spk) + escseq;
                consume (length(escseq)+1)
              end;

    if length (ch) = 1
    then case ch[1] of
          #009 : ch := specch (ktab);
          #013 : ch := specch (kent);
          esc  : ch := specch (kesc);
          #127 : ch := specch (kbksp);
          else
        end

  end;

  procedure waitch (var ch : string);
  var me:mouseevent;
  begin
    waitch (ch,me)
  end;

begin
  buffhead := 1;
  bufftail := 0
end.
