{
  This unit is ment to be used as ncurses replacement on xterm-256-color
    on ubuntu and macos/darwin to minimize bloatware.

  - colors should be set and screen cleared after init
  - getscreensize is calling ioctl with TIOCGWINSZ
  - resized flag is set via WINCH handler (installed on init and removed on done)
    to be able to respond to window resize
    . it is set to false in init (expecting first screen redraw afterwards)
  - responding to window resize (in main event loop or on keyboard reading when resized flag is set)
    . get screen size
    . redraw cols x rows screen
    . reset resized flag
  - keyboard initializaton
}

{$mode objfpc} {$longstrings on}
unit term;
interface

  procedure init;
  procedure getscreensize;
  procedure clear;
  procedure done;

  procedure move    (row,col:integer);
  procedure setfgbg (f,b:integer);
  procedure setfg   (f:integer);
  procedure setbg   (b:integer);

  procedure charset (regular:boolean);

var
  rows : integer;
  cols : integer;

  resized : boolean;



implementation

uses
  sysutils, baseunix, termio; // for winch signal handler

const
  esc = chr (27);
  csi = esc+'[';

var
  na, oa : psigactionrec; // new and old action handler record pointers
  oldtio : termios;       // termio

{$ifndef _POSIX_VDISABLE}
const
  _POSIX_VDISABLE = $ff;
  VDSUSP=11;
{$endif}

  procedure setrawmode (b:boolean);
  var Tio : Termios;
  begin
    if not b
    then TCSetAttr (0,TCSANOW,oldtio)
    else begin
           TCGetAttr (0,tio);
           oldtio := tio;
           tio.c_iflag := tio.c_iflag and not (IXON or INLCR or ICRNL);
           tio.c_lflag := tio.c_lflag and not (ICANON or ECHO or IEXTEN);

           tio.c_cc [VMIN]  := 1;
           tio.c_cc [VTIME] := 0;

           tio.c_lflag := tio.c_lflag and not ISIG;
           tio.c_cc [VQUIT]  := _POSIX_VDISABLE;
           tio.c_cc [VSUSP]  := _POSIX_VDISABLE;
           tio.c_cc [VDSUSP] := _POSIX_VDISABLE;

           TCSetAttr (0,TCSANOW,tio)
         end
  end;

  procedure sigwinchhandler (sig:cint); cdecl;
  begin
    resized := true
  end;

  procedure initsigwinchhandler;
  begin
    new (na); new (oa);
    with na^ do
    begin
      fillchar (sa_mask, sizeof(sa_mask), #0); // sa_mask := 0
      sa_handler := sigactionhandler (@sigwinchhandler);
      sa_flags := 0;
      {$ifdef linux}
      sa_restorer := nil
      {$endif}
    end;
    if fpsigaction (sigwinch, na, oa) <> 0 then
    begin writeln ('error instaling winch handler: ',fpgeterrno,'.'); halt(1) end
  end;

  procedure restoresigwinchhandler;
  begin
    if fpsigaction (sigwinch, oa, nil)<>0 then
    begin writeln ('error restoring winch handler: ',fpgeterrno,'.'); halt(1) end
  end;

  procedure getscreensize;
  var w : array [0..3] of word;
  begin
    if fpioctl (stdoutputhandle, TIOCGWINSZ, @w) <> 0
    then begin w[0]:=80; w[1]:= 24 end;
    rows := w[0]; cols := w [1]
  end;



  procedure move (row,col:integer);
  begin
    write (csi);
    if not ((row=1) and (col=1))
    then if col=1
         then write (row)
         else write (row,';',col);
    write ('H')
  end;

  procedure mouse (on:boolean);
    procedure ohl(s:string);
    begin
      write (csi,'?'+s);
      if on
      then write ('h')
      else write ('l')
    end;
  begin
    ohl('1001');
    ohl('1002');
    ohl('1006')
  end;

  procedure setfgbg (f,b:integer);
  begin
    write (csi,'38;5;',f,';48;5;',b,'m')
  end;

  procedure setfg (f:integer);
  begin
    write (csi,'38;5;',f,'m')
  end;

  procedure setbg (b:integer);
  begin
    write (csi,'48;5;',b,'m')
  end;

  procedure clear;
  begin
    move (1,1);
    write (csi+'2J')
  end;

  procedure scrolloff;
  begin
    write (csi,'1;',cols,'r')
  end;

  procedure altscreen (on:boolean);
  begin
    write (csi,'?1049');
    if on
    then write ('h')
    else write ('l')
  end;

  procedure wrap (on:boolean);
  begin
    write (csi,'?7');
    if on
    then write ('h')
    else write ('l')
  end;

  procedure cursor (on:boolean);
  begin
    write (csi,'?25');
    if on
    then write ('h')
    else write ('l')
  end;

  procedure altkeypad (on:boolean);
  begin
    if on
    then write (esc,'=')
    else write (esc,'>')
  end;

  procedure charset (regular:boolean);
  begin
    if regular
    then write (esc,'(B')
    else write (esc,'(0')
  end;

  procedure init;
  begin
    initsigwinchhandler;
    getscreensize;
    resized := false;

    term.altscreen (true); // write on alternate screen
    scrolloff;
    wrap (false);
    charset (true);    // charset us ascii
    write (csi,'m');   // default rendition
    write (csi,'4l');  // keyboard replace mode
    write (csi,'?7l'); // wrap around mode
    write (csi,'?1h'); // application cursor keys

    //write (csi,'>0;2m');  // modify keyboard keys
    write (csi,'>1;3m');  // modify cursor keys
    //write (csi,'>2;2m');  // modify function keys
    //write (csi,'>4;2m');  // modify other keys

    altkeypad (true);
    mouse (true);
    cursor (false);     // hide cursor
    setrawmode (true)
  end;

  procedure done;
  begin
    setrawmode (false);
    mouse (false);
    wrap (true);
    write (csi,'?12l'); // stop blinking cursor
    cursor (true);
    altscreen (false);
    write (csi,'?1l');  // normal cursor keys
    altkeypad (false);
    // move (cols,1);
    restoresigwinchhandler
  end;

begin
end.