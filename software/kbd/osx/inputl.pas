{$mode objfpc} {$longstrings on}
program inputl;

uses
  utf, keyboard, screen;

const
  dark  = 0;
  gray  = 8;
  light = 7;

type
  inputtype = record
                content : string;
                x,y,len : integer;
                okchars : array of widechar;
                cpos    : 0..maxcols;
                {ent, esc : string;
                lt, rt   : keychar;
                del, bsp : keychar;
                valid    : function : boolean;}
              end;

  procedure error (var s:string);
  begin
  end;

  procedure ginput (var it:inputtype);
  var
    i,l : integer;
  begin
    l := utf8len (it.content);
    setyx (it.y,it.x);
    for i := 1 to it.len do
    begin
      if i=it.cpos
      then setcol (-1,gray)
      else setcol (-1,dark);
      if i<=l
      then out (utf8ch(it.content,i))
      else out (' ')
    end;
    update
  end;

var
  it : inputtype;
  s  : utf8string;
  ch : string;

  quit : boolean;

begin
  screen.init;

  s := 'abšcd';

  with it do
  begin
    content := s;
    x := 1; y := 1;
    len := 30;
    cpos := 1
  end;

  quit := false;
  repeat
    ginput (it);
    waitch (ch);
    with it do
      case speckey (ch) of
        knone  : begin
                   content := utf8ins (content,cpos,ch);
                   if cpos<it.len then cpos := cpos + 1
                 end;
        kbksp  : if cpos>1
                 then begin
                        content := utf8del (content,cpos-1,1);
                        cpos := cpos-1
                      end;
        kdel   : content := utf8del (content,cpos,1);
        kent   : begin s := content; quit := true end;
        kesc   : quit := true;
        kleft  : if cpos>1      then cpos := cpos - 1;
        kright : if cpos<it.len then cpos := cpos + 1;
        else
      end
  until quit;

  screen.done;
  writeln (s)
end.