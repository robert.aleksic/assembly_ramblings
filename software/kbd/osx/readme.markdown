Problem to solve
----------------

Reading keyboard and writing to screen have never been as complex as today. All os's have their own ideas how it should be done, and it is ultimately complicated for programmer. In late 80's there was crt unit which enabled:
  to clear screen
  to select bg/fg color
  to positon cursor on screen
  to write characters on cusor position
  to check if key is pressed
  to read key that was pressed
  to wait until key is pressed

Unfortunately, linux, os x, various windozes are not permiting user program to do this things easily, with various excuses and justification. If you want to do it on linux/bsd based os's you can use ncourses library and programming terminal screen can then be done even through ssh/telnet channels. Unfortunately ncourses is becoming bloated, versioned and unavaible on windows. This makes cthis cross platform programmimng task even more complicated. UTF character sets, fonts etc. are making this task even more complicated.

This project is aims at providing programmer with single unit to sort out this mess.

Basic building blocks are:
 - term unit (to communicate with terminal by sending and receiving ansi esc sequences)
 - screen (to emulate display buffer, update screen via term unit) and
 - keyboard (to interpret keyboard and redraw screen when user resize terminal)


Term
----

  Depends on baseunix and termio for handling os window resize (by setting resized flag and setting cols/rows).

  - replace ncourses with small set of esc sequences for term256
  - should work through ssh

Disp
----

  Crt replacement, depends on term and keyboard.
  Handles os window resize through wait/wait(ch) loop with 10ms delay to avoid maxing processor use

  - use utf-8
  - draws on buffer with out @ curx,cury + moves cx after to enable consequential writelns
  - update just updates display to match buffer with optimized writes

Input
-----

  waitkey returns :
    - valid utf8 - for valid utf8
    - kinvutf8   - for invalid utf8
    - speccode + ord(speckeys) + unparsed esc sequence - for special keys

 tab, enter and backspace are treated as specialkeys and are never returned as keynone
 speckey returns keynone for utf-8 chars

 mouse events ...
