{$mode objfpc} {$longstrings on}
// keyboard?
// submenu, enable/disable, break, menu shortcuts, ? check,radio

program menutest;
uses
  base, utf, screen, keyboard;

const
  esc   = #27;
  enter = #10;

  dark  = 0;
  gray  = 8;
  light = 7;

type
  proc = procedure;

  menuitemref = ^menuitem;
  menuitem = record
               s : string;
               p : procedure;
               pos : integer;
               prev, next : menuitemref
             end;

  menu = record
           items : menuitemref;
           curr  : menuitemref;

           al : alignment;
           x,y : integer;
           w,h : integer
         end;

  procedure a;
  begin
    clear;
    out ('a');
    update
  end;

  procedure b;
  begin
    clear;
    out ('b');
    update
  end;

  procedure cursor (m:menu; mi:menuitemref; on:boolean=true);
  var bg : integer;
  begin
    if on then bg := gray else bg := dark;
    if mi<>nil then paint (-1,bg, m.w-2, m.y+mi^.pos, m.x+1)
  end;

  procedure initmenu (var m:menu; al:alignment=left; w:integer = -1; x:integer = 1; y:integer = 1);
  begin
    m.items := nil;
    m.curr  := nil;
    m.al    := al;

    m.x := x; m.y := y;
    m.h := 2;
    if w=-1 then m.w := 2 else m.w := w
  end;

  procedure addmenuitem (var m:menu; s:string; p:proc);
  var
    mi,last : menuitemref;
    l : integer;
  begin
    new (mi);
    mi^.s := s;
    mi^.p := p;

    m.h := m.h+1;
    l := utf8len (s);
    if (l+2) > m.w then m.w := l+2;
    if m.items = nil
    then begin
           m.items := mi;
           m.curr  := mi;
           mi^.pos := 1;
           mi^.prev := mi;
           mi^.next := mi
         end
    else begin
           last := m.items^.prev;
           last^.next := mi;

           m.items^.prev := mi;
           mi^.next := m.items;
           mi^.prev := last;
           mi^.pos  := last^.pos+1
         end
  end;

  procedure showmenu (var m:menu);
  var
    b  : array of cell;
    ch : string;

    procedure drawmenu (m:menu);
    var i : integer; mi : menuitemref;
    begin
      with m do
      begin
        setcol (light,dark);
        box (x,y,w,h);
        if curr<>nil
        then begin
               mi := items;
               i := 1;
               repeat
                 setyx (y+i,x+1);
                 out ( formatted (mi^.s,w-2,al) );
                 mi := mi^.next;
                 i := i+1
               until mi=items;
             end;
        if curr<>nil then cursor (m,curr,true);
        update
      end
    end;

  begin
    with m do
    begin
      setlength (b,w*h);
      getbox (b,x,y,w,h);

      repeat
        drawmenu (m);
        wait (ch);
        if (curr<>nil) and (length(ch)=1)
        then case ch[1] of
               'q' : curr := items;
               'a' : curr := items^.prev;
               'w' : curr := curr^.prev;
               's' : curr := curr^.next
               else
            end
      until ch='x';

      putbox (b,x,y,w,h); update;
      setlength (b,0)
    end
  end;

var
  x,y : integer;
  m : menu;

begin
  screen.init;

  for y := 1 to maxrows do
  begin
    setyx (y,1);
    for x := 1 to maxcols do
    begin
      setcol ((x-1) mod 256, (y-1) mod 256);
      out (char (((x-1) mod 10) + ord('0')))
    end
  end;
  update;

  initmenu (m,right,20,10,10);
  addmenuitem (m,'veryš long item for menu 1',@a);
  addmenuitem (m,'ano£€ther long item',@b);
  addmenuitem (m,'anotđher very very long in menu item 3',@b);

  showmenu (m);

  screen.done
end.