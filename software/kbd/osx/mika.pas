program Example8;

{ Program to demonstrate the FunctionKeyName function. }

Uses sysutils, keyboard;

Var
  K : TkeyEvent;
  r : tkeyrecord;

begin
  InitKeyboard;
  Writeln('Press function keys, press "q" to end.');
  Repeat

    k := getkeyevent;
    r := tkeyrecord(k);
    k := translatekeyevent (k);
    r := tkeyrecord(k);
    {repeat
      k:=pollkeyevent;
      sleep (10);
    until k<>0;}
    writeln ('flags: ',getkeyeventflags(k));
    writeln ('char:  ',getkeyeventchar(k));
    writeln ('unicode:  ',getkeyeventunicode(k));
    writeln ('code:  ',getkeyeventcode(k));
    writeln ('code:  ',r.keycode);
    writeln ('shiftstate:  ',getkeyevents(k));
    writeln ('shiftstate:  ',r.shiftstate);
    writeln ('flags: ',r.flags)

  Until (GetKeyEventChar(K)='q');
  DoneKeyboard;
end.