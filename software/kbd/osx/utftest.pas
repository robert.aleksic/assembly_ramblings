{$mode objfpc} {$longstrings on}
program utftest;

uses
  utf;

const
  ss = 'Š';

var
  s  : utf8string;
  ch : utf8char;
  i  : integer;

begin
  s := '[ŠĐĆČŽЖ]€šđćčž';
  for i := 1 to utf8len(s) do
  begin
    ch := utf8ch (s,i);
    write (utf8fromutf(utffromutf8(ch)))
  end;
  writeln
end.