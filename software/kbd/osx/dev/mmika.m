#!/usr/bin/swift
// swift 4.0
// compile: xcrun -sdk macosx swiftc -framework Cocoa mmika.swift -o mmika
// run: ./mmika

/*import Cocoa

	class MyObserver: NSObject {
	  override init() {
      super.init()
      NSWorkspace.shared.notificationCenter.addObserver (self,
          selector: #selector(printMe(_:)),
          name: NSWorkspace.didActivateApplicationNotification,
          object:nil)
	  }
	  @objc dynamic private func printMe(_ notification: NSNotification) {
	      let app = notification.userInfo!["NSWorkspaceApplicationKey"] as! NSRunningApplication
	      print(app.localizedName!)
	  }
	}

let observer = MyObserver()
RunLoop.main.run()
*/

/*import Foundation
import Cocoa

class FooTextField : NSTextField {

    func initMonitor() {
       NSEvent.addLocalMonitorForEvents (matching : [.keyUp, .keyDown, .flagsChanged], handler : onKeyEvent)
    }

    //prints 8 when key with keycode 42 is pressed and (⌥,⌘) are active.
    //the textfield seems to receives the correct keycode since a # appears for German keyboards
    private func onKeyEvent(_ e: NSEvent) -> NSEvent? {
      print(e.keyCode)
      return e
    }

}
*/

import Foundation
import Cocoa

let lock = DispatchQueue(label: "lock")
var events = [CGEvent]()

// track keyboard events using an event tap.
let callback: CGEventTapCallBack =  { (tapProxy, eventType, event, _) -> Unmanaged<CGEvent>? in
  // don't consume command keys.
  if !event.flags.contains(.maskCommand) {
    if eventType == .keyDown {
      lock.sync {
        events.append(event)
      }
    }
    return nil
  }
  return Unmanaged<CGEvent>.passUnretained(event)
}

let eventMask = CGEventMask((1 << CGEventType.keyDown.rawValue) | (1 << CGEventType.keyUp.rawValue))
if let frontmostProcess = NSWorkspace.shared.frontmostApplication?.processIdentifier, let eventTap = CGEvent.tapCreateForPid(pid: frontmostProcess, place: .headInsertEventTap, options: .defaultTap, eventsOfInterest: eventMask, callback: callback, userInfo: nil) {
  let source = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0)
  CFRunLoopAddSource(CFRunLoopGetCurrent(), source, .commonModes)
  CGEvent.tapEnable(tap: eventTap, enable: true)
  CFRunLoopRun()
}