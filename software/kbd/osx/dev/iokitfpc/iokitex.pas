{$mode objfpc}{$H+}
{$linkframework IOKit}

//{$modeswitch cvar}
program project1;
uses
  MacOSAll;

type
  kern_return_t = integer; // if you use "integer" then you don't need "ctypes" unit
  natural_t   = UInt32;
  mach_port_t = natural_t;
  io_object_t = mach_port_t;
  io_registry_entry_t = io_object_t;

  io_string_t = array [0..511] of ansichar;
  IOOptionBits = UInt32;

const
   kIOPlatformUUID = 'IOPlatformUUID';

var
  kIOMasterPortDefault: mach_port_t; cvar; external;

  function IORegistryEntryFromPath (masterPort : mach_port_t; const path : io_string_t) : io_registry_entry_t;
    cdecl; external;
  function IORegistryEntryCreateCFProperty (entry : io_registry_entry_t; key : CFStringRef;
                                            allocator : CFAllocatorRef; options : IOOptionBits): CFTypeRef;
    cdecl; external;

  function IOObjectRelease (entry: io_registry_entry_t) : kern_return_t; cdecl; external;

  function get_platform_uuid: string;
  var
    ioRegistryRoot : io_registry_entry_t;
    uuidCf         : CFStringRef;

    l : integer;

  begin
    ioRegistryRoot := IORegistryEntryFromPath (kIOMasterPortDefault, 'IOService:/');
    uuidCf := CFStringRef (
                IORegistryEntryCreateCFProperty (ioRegistryRoot, CFSTR (kIOPlatformUUID), kCFAllocatorDefault, 0) );
    IOObjectRelease (ioRegistryRoot);

    SetLength (Result, 1024);
    CFStringGetCString (uuidCf, @Result[1], length(Result), kCFStringEncodingMacRoman);
    CFRelease (uuidCf);

    l := StrLen (@Result[1]);
    SetLength (Result, l);
  end;

begin
  writeln('get_platform_uuid: ', get_platform_uuid);
end.