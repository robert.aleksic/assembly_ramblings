program cocoa_without_lcl;

{$mode objfpc}{$H+}
{$modeswitch objectivec1}

uses
  CocoaAll, classes, sysutils;

type
  { TMyDelegate }
  TMyDelegate = objcclass(NSObject)
  public
    procedure HandleButtonClick_A(sender: id); message 'HandleButtonClick_A:';
    procedure HandleButtonClick_B(sender: id); message 'HandleButtonClick_B:';
    procedure HandleMenuClick_A(sender: id); message 'HandleMenuClick_A:';
    procedure HandleMenuClick_B(sender: id); message 'HandleMenuClick_B:';
  end;

var
  appName: NSString;
  window: NSWindow;
  pool: NSAutoreleasePool;
  lText: NSTextField;
  lButton: NSButton;
  lDelegate: TMyDelegate;
  //
  MainMenu_A: NSMenu;
  TopItem_A: NSMenuItem;
  TopMenu_A: NSMenu;
  MenuItem_A: NSMenuItem;
  //
  MainMenu_B: NSMenu;
  TopItem_B: NSMenuItem;
  TopMenu_B: NSMenu;
  MenuItem_B: NSMenuItem;

procedure TMyDelegate.HandleButtonClick_A(sender: id);
begin
  NSApp.setMainMenu(MainMenu_A);
end;

procedure TMyDelegate.HandleButtonClick_B(sender: id);
begin
  NSApp.setMainMenu(MainMenu_B);
end;

procedure TMyDelegate.HandleMenuClick_A(sender: id);
var
  Str: string;
begin
  Str := 'A '+inttostr(random(888));
  window.setTitle(NSSTR(@Str[1]));
end;

procedure TMyDelegate.HandleMenuClick_B(sender: id);
var
  Str: string;
begin
  Str := 'B '+inttostr(random(888));
  window.setTitle(NSSTR(@Str[1]));
end;

procedure CreateMenus;
var
  nsTitle: NSString;
  nsKey  : NSString;

  function CreateMenuItem(AName, AShortcut: string; ASelector: SEL): NSMenuItem;
  begin
    nsKey := NSSTR(PChar(AShortcut));
    nsTitle := NSSTR(PChar(AName));
    Result := NSMenuItem.alloc.initWithTitle_action_keyEquivalent(nsTitle, ASelector, nsKey);
    Result.setKeyEquivalentModifierMask(NSCommandKeyMask);
    nsTitle.release;
    nsKey.release;
    Result.setKeyEquivalentModifierMask(NSCommandKeyMask);
    Result.setTarget(lDelegate);
  end;

begin
  MainMenu_A := NSMenu.alloc.initWithTitle(NSSTR('Menu A'));
  TopItem_A := CreateMenuItem('TopItem A', '', nil);
  MainMenu_A.insertItem_atIndex(TopItem_A, 0);
  TopMenu_A := NSMenu.alloc.initWithTitle(NSSTR('TopMenu A'));
  TopItem_A.setSubmenu(TopMenu_A);
  MenuItem_A := CreateMenuItem('MenuItem A', 'A', objcselector('HandleMenuClick_A:'));
  MenuItem_A.setTarget(lDelegate);
  TopMenu_A.insertItem_atIndex(MenuItem_A, 0);

  MainMenu_B := NSMenu.alloc.initWithTitle(NSSTR('Menu B'));
  TopItem_B := CreateMenuItem('TopItem B', '', nil);
  MainMenu_B.insertItem_atIndex(TopItem_B, 0);
  TopMenu_B := NSMenu.alloc.initWithTitle(NSSTR('TopMenu B'));
  TopItem_B.setSubmenu(TopMenu_B);
  MenuItem_B := CreateMenuItem('MenuItem B', 'B', objcselector('HandleMenuClick_B:'));
  MenuItem_B.setTarget(lDelegate);
  TopMenu_B.insertItem_atIndex(MenuItem_B, 0);
end;


begin
  // Autorelease pool, app and window creation
  pool := NSAutoreleasePool.new;
  NSApp := NSApplication.sharedApplication;
  NSApp.setActivationPolicy(NSApplicationActivationPolicyRegular);
  appName := NSProcessInfo.processInfo.processName;
  window := NSWindow.alloc.initWithContentRect_styleMask_backing_defer(NSMakeRect(0, 0, 200, 200),
    NSTitledWindowMask or NSClosableWindowMask or NSMiniaturizableWindowMask,
    NSBackingStoreBuffered, False).autorelease;
  lDelegate := TMyDelegate.alloc.init;

  // text label
  lText := NSTextField.alloc.initWithFrame(NSMakeRect(50, 50, 120, 50)).autorelease;
  lText.setBezeled(False);
  lText.setDrawsBackground(False);
  lText.setEditable(False);
  lText.setSelectable(False);
  lText.setStringValue(NSSTR('NSTextField'));
  window.contentView.addSubview(lText);

  // button
  lButton := NSButton.alloc.initWithFrame(NSMakeRect(50, 100, 120, 50)).autorelease;
  window.contentView.addSubview(lButton);
  lButton.setTitle(NSSTR('Button A!'));
  lButton.setButtonType(NSMomentaryLightButton);
  lButton.setBezelStyle(NSRoundedBezelStyle);
  // button event handler setting
  lButton.setTarget(lDelegate);
  lButton.setAction(ObjCSelector(lDelegate.HandleButtonClick_A));

  // button B
  lButton := NSButton.alloc.initWithFrame(NSMakeRect(50, 150, 120, 50)).autorelease;
  window.contentView.addSubview(lButton);
  lButton.setTitle(NSSTR('Button B!'));
  lButton.setButtonType(NSMomentaryLightButton);
  lButton.setBezelStyle(NSRoundedBezelStyle);
  // button event handler setting
  lButton.setTarget(lDelegate);
  lButton.setAction(ObjCSelector(lDelegate.HandleButtonClick_B));

  // Menus
  CreateMenus();
  NSApp.setMainMenu(MainMenu_A);

  // Window showing and app running
  window.center;
  window.setTitle(appName);
  window.makeKeyAndOrderFront(nil);
  NSApp.activateIgnoringOtherApps(true);
  NSApp.run;
end.