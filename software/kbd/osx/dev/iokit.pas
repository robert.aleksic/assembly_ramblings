{$mode objfpc}{$H+}
{$linkframework IOKit}

program iokit;
uses
  MacOSXPosix, kern_return, IOKitReturn;

const
  MACH_PORT_NULL : mach_port_t = 0;

type
  IONotificationPortRef = pointer;

var
  npref : IONotificationPortRef;

  function IOMasterPort (bootstrapPort : mach_port_t; var masterPort : mach_port_t) : kern_return_t; cdecl; external;

  function  IONotificationPortCreate  (masterPort   : mach_port_t) : IONotificationPortRef; cdecl; external;
  procedure IONotificationPortDestroy (notification : IONotificationPortRef); cdecl; external;


  function isnil (p:pointer) : boolean;
  begin isnil := (ptruint(p) = 0) or ((not ptruint(p)) = 0) or (p=nil) end;

  function notnil (p:pointer) : boolean;
  begin notnil := not isnil (p) end;

  procedure init;
  var
    kr : kern_return_t;
    mp  : mach_port_t;

  begin
    npref := nil;
    kr := IOMasterPort (MACH_PORT_NULL, mp);

    if kr = kIOReturnSuccess
    then npref := IONotificationPortCreate (mp);

    if notnil (npref)
    then begin

         end;
    writeln (ptruint(npref))
  end;

  procedure cleanup;
  begin
    IONotificationPortDestroy (npref);
    npref := nil
  end;

begin
  init;
  cleanup
end.