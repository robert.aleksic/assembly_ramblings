// gcc clean.c -framework IOKit -framework CoreFoundation

//#include <CoreFoundation/CoreFoundation.h>
//#include <IOKit/IOKitLib.h>
//#include <IOKit/IOMessage.h>
#include <IOKit/IOCFPlugIn.h>
#include <IOKit/hid/IOHIDLib.h>
//#include <IOKit/hid/IOHIDKeys.h>
//#include <IOKit/hid/IOHIDUsageTables.h>
//#include <IOKit/hidsystem/IOHIDLib.h>
//#include <IOKit/hidsystem/IOHIDShared.h>
//#include <IOKit/hidsystem/IOHIDParameter.h>

typedef struct HIDData {
  io_object_t                 notification;
  IOHIDDeviceInterface122 **  hidDeviceInterface;
  IOHIDQueueInterface **      hidQueueInterface;
  CFDictionaryRef             hidElementDictionary;
  CFRunLoopSourceRef          eventSource;
  UInt8                       buffer [256];
} HIDData;
typedef HIDData* HIDDataRef;

typedef struct HIDElement {
  SInt32              currentValue;
  SInt32              usagePage;
  SInt32              usage;
  IOHIDElementType    type;
  IOHIDElementCookie  cookie;
  HIDDataRef          owner;
} HIDElement;
typedef HIDElement* HIDElementRef;

static IONotificationPortRef  gNotifyPort = NULL;
static io_iterator_t          gAddedIter  = 0;



//---------------------------------------------------------------------------
// InterruptReportCallbackFunction
//---------------------------------------------------------------------------
static void InterruptReportCallbackFunction
              (void *       target,
               IOReturn     result,
               void *       refcon,
               void *       sender,
               uint32_t     bufferSize) {

  HIDDataRef  hidDataRef;
  int         index;

  hidDataRef = (HIDDataRef) refcon;
  if (hidDataRef) {
    printf("Buffer = ");
    for (index=0; index<bufferSize; index++)
      printf("%2.2x ", hidDataRef->buffer[index]);
    printf("\n");
  }
}

//---------------------------------------------------------------------------
// DeviceNotification
//
// This routine will get called whenever any kIOGeneralInterest notification
// happens.
//---------------------------------------------------------------------------
static void DeviceNotification (void *        refCon,
                                io_service_t  service,
                                natural_t     messageType,
                                void *        messageArgument) {
  kern_return_t  kr;
  HIDDataRef     hidDataRef = (HIDDataRef) refCon;

  /* Check to see if a device went away and clean up. */
  if ( (hidDataRef != NULL) && (messageType == kIOMessageServiceIsTerminated) ) {
    if (hidDataRef->hidQueueInterface != NULL) {
      kr = (*(hidDataRef->hidQueueInterface)) -> stop    (hidDataRef->hidQueueInterface);
      kr = (*(hidDataRef->hidQueueInterface)) -> dispose (hidDataRef->hidQueueInterface);
      kr = (*(hidDataRef->hidQueueInterface)) -> Release (hidDataRef->hidQueueInterface);
      hidDataRef->hidQueueInterface = NULL;
    }
    if (hidDataRef->hidDeviceInterface != NULL) {
      kr = (*(hidDataRef->hidDeviceInterface)) -> close   (hidDataRef->hidDeviceInterface);
      kr = (*(hidDataRef->hidDeviceInterface)) -> Release (hidDataRef->hidDeviceInterface);
      hidDataRef->hidDeviceInterface = NULL;
    }
    if (hidDataRef->notification) {
      kr = IOObjectRelease (hidDataRef->notification);
      hidDataRef->notification = 0;
    }
  }
}

//---------------------------------------------------------------------------
// FindHIDElements
//---------------------------------------------------------------------------
static bool FindHIDElements (HIDDataRef hidDataRef) {

  bool result = true;

  CFMutableDictionaryRef  hidElements  = NULL;
  IOReturn                ret          = kIOReturnError;

  CFArrayRef elementArray  = NULL;

  unsigned        i;
  CFDictionaryRef element    = NULL;
  HIDElement      newElement;

  CFNumberRef      number  = NULL;
  CFMutableDataRef newData = NULL;

  if (!hidDataRef) return false;

  /* Create a mutable dictionary to hold HID elements. */
  hidElements = CFDictionaryCreateMutable (kCFAllocatorDefault, 0,
                                          &kCFTypeDictionaryKeyCallBacks,
                                          &kCFTypeDictionaryValueCallBacks);
  if (!hidElements) return false;

  // Let's find the elements
  ret = (*hidDataRef->hidDeviceInterface) -> copyMatchingElements (hidDataRef->hidDeviceInterface,
                                                                   NULL, &elementArray);
  if ((ret == kIOReturnSuccess) && elementArray) {

    //CFShow(elementArray);
    /* Iterate through the elements and read their values. */
    for (i=0; i<CFArrayGetCount (elementArray); i++) {
      element = (CFDictionaryRef) CFArrayGetValueAtIndex (elementArray, i);
      if (!element) continue;
      bzero (&newElement, sizeof(HIDElement));
      newElement.owner = hidDataRef;

      /* Read the element's usage page (top level category describing the type of
         element---kHIDPage_GenericDesktop, for example) */
      number = (CFNumberRef) CFDictionaryGetValue (element, CFSTR (kIOHIDElementUsagePageKey));
      if ( !number ) continue;
      CFNumberGetValue (number, kCFNumberSInt32Type, &newElement.usagePage );

      /* Read the element's usage (second level category describing the type of
        element---kHIDUsage_GD_Keyboard, for example) */
      number = (CFNumberRef) CFDictionaryGetValue (element, CFSTR (kIOHIDElementUsageKey));
      if ( !number ) continue;
      CFNumberGetValue (number, kCFNumberSInt32Type, &newElement.usage );

      /* Read the cookie (unique identifier) for the element */
      number = (CFNumberRef) CFDictionaryGetValue (element, CFSTR (kIOHIDElementCookieKey));
      if ( !number ) continue;
      CFNumberGetValue (number, kCFNumberIntType, &(newElement.cookie) );

      /* Determine what type of element this is---button, Axis, etc. */
      number = (CFNumberRef) CFDictionaryGetValue (element, CFSTR (kIOHIDElementTypeKey));
      if ( !number ) continue;
      CFNumberGetValue(number, kCFNumberIntType, &(newElement.type) );

      /* Pay attention to X/Y coordinates of a pointing device and
         the first mouse button.  For other elements, go on to the
         next element. */
      if (newElement.usagePage == kHIDPage_GenericDesktop) {
        switch (newElement.usage) {
          case kHIDUsage_GD_X:
          case kHIDUsage_GD_Y:
          break;

          default:
          continue;
        }
      }
      else
        if (newElement.usagePage == kHIDPage_Button){
          switch (newElement.usage) {
            case kHIDUsage_Button_1:
            break;

            default:
            continue;
          }
        }
        else continue;

      /* Add this element to the hidElements dictionary. */
      newData = CFDataCreateMutable (kCFAllocatorDefault, sizeof (HIDElement));
      if (!newData) continue;
      bcopy (&newElement, CFDataGetMutableBytePtr (newData), sizeof (HIDElement));

      number = CFNumberCreate (kCFAllocatorDefault, kCFNumberIntType, &newElement.cookie);
      if (!number) continue;

      CFDictionarySetValue (hidElements, number, newData);

      CFRelease (number);
      CFRelease (newData);
    }
  }

  if (elementArray) CFRelease (elementArray);

  if (CFDictionaryGetCount (hidElements) != 0)
    hidDataRef->hidElementDictionary = hidElements;
  else {
    CFRelease (hidElements);
    hidElements = NULL; }

  return hidDataRef->hidElementDictionary;
}

//---------------------------------------------------------------------------
// HIDDeviceAdded
//
// This routine is the callback for our IOServiceAddMatchingNotification.
// When we get called we will look at all the devices that were added and
// we will:
//
//   - Create some private data to relate to each device
//   - Submit an IOServiceAddInterestNotification of type kIOGeneralInterest for
//     this device using the refCon field to store a pointer to our private data.
//
// When we get called with this interest notification, we can grab the refCon
// and access our private data.
//-------------------------------------------------------------------------------
static void HIDDeviceAdded (void *refCon, io_iterator_t iterator) {

  io_object_t           hidDevice        = 0;
  IOCFPlugInInterface** plugInInterface  = NULL;
  SInt32                score;
  IOReturn              kr;

  IOHIDDeviceInterface122** hidDeviceInterface  = NULL;
  HRESULT                   result              = S_FALSE;

  HIDDataRef  hidDataRef = NULL;
  CFArrayRef  arrayRef;
  CFDataRef   data;

  const UInt8* buffer;
  int          i;

  /* Interate through all the devices that matched */
  while ( (hidDevice = IOIteratorNext (iterator)) ) {

    // Create the CF plugin for this device
    kr = IOCreatePlugInInterfaceForService (hidDevice, kIOHIDDeviceUserClientTypeID,
                                            kIOCFPlugInInterfaceID, &plugInInterface, &score);
    if (kr == kIOReturnSuccess) {
      /* Obtain a device interface structure (hidDeviceInterface). */
      result = (*plugInInterface) -> QueryInterface (plugInInterface,
                                                     CFUUIDGetUUIDBytes (kIOHIDDeviceInterfaceID122),
                                                     (LPVOID *) &hidDeviceInterface);
      // Got the interface
      if ((result != S_OK) || !hidDeviceInterface) { // Failed to allocate a UPS interface, cleanup
        if (hidDeviceInterface) {
          (*hidDeviceInterface) -> Release(hidDeviceInterface);
          hidDeviceInterface = NULL;
        }
      }
      else {
        /* Create a custom object to keep data around for later. */
        hidDataRef = malloc (sizeof (HIDData));
        bzero (hidDataRef, sizeof (HIDData));
        hidDataRef->hidDeviceInterface = hidDeviceInterface;

        /* Check for Bluetooth HID descriptors first. */
        /* Create a CFArray object based on the IORegistry entry for this HID Device. */
        arrayRef = IORegistryEntryCreateCFProperty(hidDevice, CFSTR("HIDDescriptor"), 0, 0);

        if (arrayRef) {
          CFShow (arrayRef);
          arrayRef = CFArrayGetValueAtIndex (arrayRef, 0);
          if (arrayRef) {
            CFShow (arrayRef);
            data = CFArrayGetValueAtIndex (arrayRef, 1);
            CFShow (data);

            if (data) {
              buffer = CFDataGetBytePtr (data);
              printf ("Bluetooth HID descriptor: ");
              for (i=0; i<CFDataGetLength (data); i++) printf ("0x%x, ", buffer[i]);
              printf ("\n");
            }
          }
        }

        /* Open the device. */
        result = (*(hidDataRef->hidDeviceInterface)) -> open (hidDataRef->hidDeviceInterface, 0);

        /* Find the HID elements for this device */
        result = FindHIDElements (hidDataRef);

        /* Create an asynchronous event source for this device. */
        result = (*(hidDataRef->hidDeviceInterface)) ->
            createAsyncEventSource (hidDataRef->hidDeviceInterface, &hidDataRef->eventSource);

        /* Set the handler to call when the device sends a report. */
        result = (*(hidDataRef->hidDeviceInterface)) ->
            setInterruptReportHandlerCallback (hidDataRef->hidDeviceInterface,
                                               hidDataRef->buffer, sizeof (hidDataRef->buffer),
                                               &InterruptReportCallbackFunction, NULL, hidDataRef);

        /* Add the asynchronous event source to the run loop. */
        CFRunLoopAddSource (CFRunLoopGetCurrent(), hidDataRef->eventSource, kCFRunLoopDefaultMode);

        /* Register an interest in finding out anything that happens with this device (disconnection, for example) */
        IOServiceAddInterestNotification (
                                  gNotifyPort,                  // notifyPort
                                  hidDevice,                    // service
                                  kIOGeneralInterest,           // interestType
                                  DeviceNotification,           // callback
                                  hidDataRef,                   // refCon
                                  &(hidDataRef->notification)); // notification

        // Clean up
        if (hidDataRef) free (hidDataRef);
      }

      (*plugInInterface) -> Release (plugInInterface);
    }

    IOObjectRelease (hidDevice);
  }
}

//---------------------------------------------------------------------------
// InitHIDNotifications
//
// This routine just creates our master port for IOKit and turns around
// and calls the routine that will alert us when a HID Device is plugged in.
//---------------------------------------------------------------------------

static void InitHIDNotifications () {
  kern_return_t           kr;
  mach_port_t             masterPort;
  CFMutableDictionaryRef  matchingDict;

  CFNumberRef    refProdID;
  CFNumberRef    refVendorID;
  SInt32         productID = 1; // 523; //   1;
  SInt32         vendorID  = 6; //1452; //1616;

  // first create a master_port for my task
  kr = IOMasterPort (MACH_PORT_NULL, &masterPort);
  if (kr || !masterPort) return;

  // Create a notification port and add its run loop event source to our run loop
  // This is how async notifications get set up.
  gNotifyPort = IONotificationPortCreate (masterPort);
  CFRunLoopAddSource (CFRunLoopGetCurrent(),
                      IONotificationPortGetRunLoopSource (gNotifyPort),
                      kCFRunLoopDefaultMode);

  // Create the IOKit notifications that we need

  /* Create a matching dictionary that (initially) matches all HID devices. */
  matchingDict = IOServiceMatching ("IOHIDDevice");
  printf ("%x\n",matchingDict);

  if (matchingDict) {
    /* Create objects for product and vendor IDs. */
    refProdID   = CFNumberCreate (kCFAllocatorDefault, kCFNumberIntType, &productID);
    refVendorID = CFNumberCreate (kCFAllocatorDefault, kCFNumberIntType, &vendorID);
    /* Add objects to matching dictionary and clean up. */
    CFDictionarySetValue (matchingDict, CFSTR (kIOHIDPrimaryUsageKey),     refVendorID);
    CFDictionarySetValue (matchingDict, CFSTR (kIOHIDPrimaryUsagePageKey), refProdID);
    CFRelease (refVendorID);
    CFRelease (refProdID);

    // Now set up a notification to be called when a device is first matched by I/O Kit.
    // Note that this will not catch any devices that were already plugged in so we take
    // care of those later.
    kr = IOServiceAddMatchingNotification (gNotifyPort,                // notifyPort
                                           kIOFirstMatchNotification,  // notificationType
                                           matchingDict,               // matching
                                           HIDDeviceAdded,             // callback
                                           NULL,                       // refCon
                                           &gAddedIter);               // notification
    if ( kr == kIOReturnSuccess ) HIDDeviceAdded (NULL, gAddedIter);
  }
}

int main (int argc, const char * argv[]) {
  if (argc == 1) {
    InitHIDNotifications ();
    CFRunLoopRun ();
    printf ("done\n");
  }
  return 0;
}