/*
 File: main.c

 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.

 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.

 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.

 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

 Copyright (C) 2013 Apple Inc. All Rights Reserved.

 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/errno.h>
#include <sysexits.h>
#include <mach/mach.h>
#include <mach/mach_error.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/IOCFPlugIn.h>
#include <IOKit/hid/IOHIDLib.h>
#include <IOKit/hid/IOHIDKeys.h>
// #include <Carbon/Carbon.h>
#include <CoreFoundation/CoreFoundation.h>

typedef struct cookie_struct
{
	IOHIDElementCookie gXAxisCookie;
	IOHIDElementCookie gButton1Cookie;
	IOHIDElementCookie gButton2Cookie;
	IOHIDElementCookie gButton3Cookie;
} *cookie_struct_t;

#include "hidprotos.h"

static void print_errmsg_if_io_err(int expr, char *msg)
{
	IOReturn err = (expr);
	if (err != kIOReturnSuccess) {
		fprintf(stderr, "%s - %s(%x,%d)\n", msg,
								mach_error_string(err),
								err, err & 0xffffff);
		fflush(stderr);
		exit(EX_OSERR);
	}
}

static void print_errmsg_if_err(int expr, char *msg)
{
	if (expr) {
		fprintf(stderr, "%s\n", msg);
		fflush(stderr);
		exit(EX_OSERR);
	}
}


static void MyStartHIDDeviceInterfaceTest(void)
{
	io_iterator_t hidObjectIterator = 0;

	MyFindHIDDevices(kIOMasterPortDefault, &hidObjectIterator);

	if (hidObjectIterator)
	{
		MyTestHIDDevices(hidObjectIterator);

		//Release iterator. Don't need to release iterator objects.
		IOObjectRelease(hidObjectIterator);
	}

}

void MyFindHIDDevices(mach_port_t masterPort,
				io_iterator_t *hidObjectIterator)
{
    CFMutableDictionaryRef hidMatchDictionary = NULL;
    IOReturn ioReturnValue = kIOReturnSuccess;
    Boolean noMatchingDevices = false;

    // Set up a matching dictionary to search the I/O Registry by class
	// name for all HID class devices
	hidMatchDictionary = IOServiceMatching(kIOHIDDeviceKey);

 	// Now search I/O Registry for matching devices.
 	ioReturnValue = IOServiceGetMatchingServices(masterPort,
 							hidMatchDictionary, hidObjectIterator);

 	noMatchingDevices = ((ioReturnValue != kIOReturnSuccess)
 								| (*hidObjectIterator == 0));

	//If search is unsuccessful, print message and hang.
	if (noMatchingDevices)
		print_errmsg_if_err(ioReturnValue, "No matching HID class devices found.");

 	// IOServiceGetMatchingServices consumes a reference to the
 	//   dictionary, so we don't need to release the dictionary ref.
 	hidMatchDictionary = NULL;
}

static void MyShowHIDProperties(io_registry_entry_t hidDevice)
{
	kern_return_t						result;
	CFMutableDictionaryRef						properties = 0;
	char						path[512];

	result = IORegistryEntryGetPath(hidDevice,
									kIOServicePlane,
									path);
	if (result == KERN_SUCCESS)
		printf("[ %s ]", path);

	//Create a CF dictionary representation of the I/O
	//Registry entry's properties
	result = IORegistryEntryCreateCFProperties(hidDevice,
											&properties,
											kCFAllocatorDefault,
											kNilOptions);
	if ((result == KERN_SUCCESS) && properties)
	{
		CFShow(properties);
                /* Some common properties of interest include:
			kIOHIDTransportKey, kIOHIDVendorIDKey,
			kIOHIDProductIDKey, kIOHIDVersionNumberKey,
			kIOHIDManufacturerKey, kIOHIDProductKey,
			kIOHIDSerialNumberKey, kIOHIDLocationIDKey,
			kIOHIDPrimaryUsageKey, kIOHIDPrimaryUsagePageKey,
			and kIOHIDElementKey.
                */

		//Release the properties dictionary
		CFRelease(properties);
	}
	printf("\n\n");
}

cookie_struct_t getHIDCookies(IOHIDDeviceInterface122 **handle)
{
	cookie_struct_t cookies = memset(malloc(sizeof(*cookies)), 0, sizeof(*cookies));
	CFTypeRef					object;
	long					number;
	IOHIDElementCookie					cookie;
	long					usage;
	long					usagePage;
	CFArrayRef				elements;
	CFDictionaryRef				element;
	IOReturn success;

	if (!handle || !(*handle)) return cookies;

	// Copy all elements, since we're grabbing most of the elements
	// for this device anyway, and thus, it's faster to iterate them
	// ourselves. When grabbing only one or two elements, a matching
	// dictionary should be passed in here instead of NULL.
	success = (*handle)->copyMatchingElements(handle, NULL, &elements);

	printf("LOOKING FOR ELEMENTS.\n");
	if (success == kIOReturnSuccess) {
		CFIndex i;
		printf("ITERATING...\n");
		for (i=0; i<CFArrayGetCount(elements); i++)
		{
			element = CFArrayGetValueAtIndex(elements, i);
			// printf("GOT ELEMENT.\n");

			//Get cookie
			object = (CFDictionaryGetValue(element, CFSTR(kIOHIDElementCookieKey)));
			if (object == 0 || CFGetTypeID(object) != CFNumberGetTypeID())
				continue;
			if(!CFNumberGetValue((CFNumberRef) object, kCFNumberLongType, &number))
				continue;
			cookie = (IOHIDElementCookie) number;

			//Get usage
			object = CFDictionaryGetValue(element, CFSTR(kIOHIDElementUsageKey));
			if (object == 0 || CFGetTypeID(object) != CFNumberGetTypeID())
				continue;
			if (!CFNumberGetValue((CFNumberRef) object, kCFNumberLongType, &number))
				continue;
			usage = number;

			//Get usage page
			object = CFDictionaryGetValue(element,CFSTR(kIOHIDElementUsagePageKey));
			if (object == 0 || CFGetTypeID(object) != CFNumberGetTypeID())
				continue;
			if (!CFNumberGetValue((CFNumberRef) object, kCFNumberLongType, &number))
				continue;
			usagePage = number;

			//Check for x axis
			if (usage == 0x30 && usagePage == 0x01)
				cookies->gXAxisCookie = cookie;
			//Check for buttons
			else if (usage == 0x01 && usagePage == 0x09)
				cookies->gButton1Cookie = cookie;
			else if (usage == 0x02 && usagePage == 0x09)
				cookies->gButton2Cookie = cookie;
			else if (usage == 0x03 && usagePage == 0x09)
				cookies->gButton3Cookie = cookie;
		}
		printf("DONE.\n");
	} else {
		printf("copyMatchingElements failed with error %d\n", success);
	}

	return cookies;
}

static void MyCreateHIDDeviceInterface(io_object_t hidDevice,
							IOHIDDeviceInterface ***hidDeviceInterface)
{
	io_name_t						className;
	IOCFPlugInInterface						**plugInInterface = NULL;
	HRESULT						plugInResult = S_OK;
	SInt32						score = 0;
	IOReturn						ioReturnValue = kIOReturnSuccess;

	ioReturnValue = IOObjectGetClass(hidDevice, className);

	print_errmsg_if_io_err(ioReturnValue, "Failed to get class name.");

	printf("Found device type %s\n", className);

	ioReturnValue = IOCreatePlugInInterfaceForService(hidDevice,
							kIOHIDDeviceUserClientTypeID,
							kIOCFPlugInInterfaceID,
							&plugInInterface,
							&score);

	if (ioReturnValue == kIOReturnSuccess)
	{
		//Call a method of the intermediate plug-in to create the device
		//interface
		plugInResult = (*plugInInterface)->QueryInterface(plugInInterface,
							CFUUIDGetUUIDBytes(kIOHIDDeviceInterfaceID),
							(LPVOID *) hidDeviceInterface);
		print_errmsg_if_err(plugInResult != S_OK, "Couldn't create HID class device interface");

		(*plugInInterface)->Release(plugInInterface);
	}
}

void MyTestHIDInterface(IOHIDDeviceInterface ** hidDeviceInterface, cookie_struct_t cookies)
{
        HRESULT                                 result;
        IOHIDEventStruct                                        hidEvent;
        long                                    index;

        printf("X Axis (%lx), Button 1 (%lx), Button 2 (%lx), Button 3 (%lx)\n",
                        (long) cookies->gXAxisCookie, (long) cookies->gButton1Cookie,
                        (long) cookies->gButton2Cookie, (long) cookies->gButton3Cookie);

        for (index = 0; index < 10; index++)
        {
                long xAxis, button1, button2, button3;

                //Get x axis
                result = (*hidDeviceInterface)->getElementValue(hidDeviceInterface,
                                                                                        cookies->gXAxisCookie, &hidEvent);
                if (result)                     printf("getElementValue error = %lx", result);
                xAxis = hidEvent.value;

                //Get button 1
                result = (*hidDeviceInterface)->getElementValue(hidDeviceInterface,
                                                                                        cookies->gButton1Cookie, &hidEvent);
                if (result)                     printf("getElementValue error = %lx", result);
                button1 = hidEvent.value;

                //Get button 2
                result = (*hidDeviceInterface)->getElementValue(hidDeviceInterface,
                                                                                        cookies->gButton2Cookie, &hidEvent);
                if (result)                     printf("getElementValue error = %lx", result);
                button2 = hidEvent.value;

                //Get button 3
                result = (*hidDeviceInterface)->getElementValue(hidDeviceInterface,
                                                                                        cookies->gButton3Cookie, &hidEvent);
                if (result)                     printf("getElementValue error = %lx", result);
                button3 = hidEvent.value;

                //Print values
                printf("%ld %s%s%s\n", xAxis, button1 ? "button1 " : "",
                                button2 ? "button2 " : "", button3 ? "button3 " : "");

                sleep(1);
        }
}

static void MyTestHIDDeviceInterface(IOHIDDeviceInterface **hidDeviceInterface, cookie_struct_t cookies)
{
	IOReturn ioReturnValue = kIOReturnSuccess;

	//open the device
	ioReturnValue = (*hidDeviceInterface)->open(hidDeviceInterface, 0);
	printf("Open result = %d\n", ioReturnValue);

	//test queue interface
	MyTestQueues(hidDeviceInterface, cookies);

	//test the interface
	MyTestHIDInterface(hidDeviceInterface, cookies);

	//close the device
	if (ioReturnValue == KERN_SUCCESS)
		ioReturnValue = (*hidDeviceInterface)->close(hidDeviceInterface);

	//release the interface
	(*hidDeviceInterface)->Release(hidDeviceInterface);
}

void MyTestQueues(IOHIDDeviceInterface **hidDeviceInterface, cookie_struct_t cookies)
{
	HRESULT 						result;
	IOHIDQueueInterface **						queue;
	Boolean						hasElement;
	long						index;
	IOHIDEventStruct						event;

	queue = (*hidDeviceInterface)->allocQueue(hidDeviceInterface);

	if (queue)
	{
		printf("Queue allocated: %lx\n", (long) queue);
		//create the queue
		result = (*queue)->create(queue,
								 0,
								 8);	/* depth: maximum number of elements
									in queue before oldest elements in
									queue begin to be lost. */
		printf("Queue create result: %lx\n", result);

		//add elements to the queue
		result = (*queue)->addElement(queue, cookies->gXAxisCookie, 0);
		printf("Queue added x axis result: %lx\n", result);
		result = (*queue)->addElement(queue, cookies->gButton1Cookie, 0);
		printf("Queue added button 1 result: %lx\n", result);
		result = (*queue)->addElement(queue, cookies->gButton2Cookie, 0);
		printf("Queue added button 2 result: %lx\n", result);
		result = (*queue)->addElement(queue, cookies->gButton3Cookie, 0);
		printf("Queue added button 3 result: %lx\n", result);


		//check to see if button 3 is in queue
		hasElement = (*queue)->hasElement(queue,cookies->gButton3Cookie);
		printf("Queue has button 3 result: %s\n", hasElement ? "true" : "false");

		//remove button 3 from queue
		result = (*queue)->removeElement(queue, cookies->gButton3Cookie);
		printf("Queue remove button 3 result: %lx\n",result);

		//start data delivery to queue
		result = (*queue)->start(queue);
		printf("Queue start result: %lx\n", result);

		//check queue a few times to see accumulated events
		sleep(1);
		printf("Checking queue\n");
		for (index = 0; index < 10; index++)
		{
			AbsoluteTime				zeroTime = {0,0};

			result = (*queue)->getNextEvent(queue, &event, zeroTime, 0);
			if (result)
				printf("Queue getNextEvent result: %lx\n", result);
			else
				printf("Queue: event:[%lx] %ld\n",
								(unsigned long) event.elementCookie,
								(long)event.value);

			sleep(1);
		}

		//stop data delivery to queue
		result = (*queue)->stop(queue);
		printf("Queue stop result: %lx\n", result);

		//dispose of queue
		result = (*queue)->dispose(queue);
		printf("Queue dispose result: %lx\n", result);

		//release the queue we allocated
		(*queue)->Release(queue);
	}
}

void MyTestHIDDevices(io_iterator_t hidObjectIterator)
{
	io_object_t						hidDevice = 0;
	IOHIDDeviceInterface						**hidDeviceInterface = NULL;
	IOReturn						ioReturnValue = kIOReturnSuccess;

	while ((hidDevice = IOIteratorNext(hidObjectIterator)))
	{
		cookie_struct_t cookies;
		MyShowHIDProperties(hidDevice);

		MyCreateHIDDeviceInterface(hidDevice, &hidDeviceInterface);

		cookies = getHIDCookies((IOHIDDeviceInterface122 **)hidDeviceInterface);
		ioReturnValue = IOObjectRelease(hidDevice);
		print_errmsg_if_io_err(ioReturnValue, "Error releasing HID device");
		if (hidDeviceInterface != NULL)
		{
			MyTestHIDDeviceInterface(hidDeviceInterface, cookies);
			(*hidDeviceInterface)->Release(hidDeviceInterface);
		}
	}
	IOObjectRelease(hidObjectIterator);
}

int main (int argc, const char * argv[]) {
    // insert code here...
    // CFShow(CFSTR("Hello, World!\n"));
	MyStartHIDDeviceInterfaceTest();
    return 0;
}

static void QueueCallbackFunction(
                            void * 											target,
                            IOReturn 												result,
                            void * 											refcon,
                            void * 											sender)
{
}

bool addQueueCallbacks( IOHIDQueueInterface **hidQueueInterface)
{
	IOReturn ret;
	CFRunLoopSourceRef 		eventSource;
	/*	We could use any data structure here. This data structure
		will be passed to the callback, and should probably
		include some information about the queue, assuming your
		program deals with more than one. */
	IOHIDQueueInterface ***myPrivateData = malloc(sizeof(*myPrivateData));
	*myPrivateData = hidQueueInterface;

	// In the calling function, we did something like:
	// hidQueueInterface = (*hidDeviceInterface)->
 	// 		allocQueue(hidDeviceInterface);
	// (*hidQueueInterface)->create(hidQueueInterface, 0, 8);
	ret = (*hidQueueInterface)->
		createAsyncEventSource(hidQueueInterface,
		&eventSource);
	if ( ret != kIOReturnSuccess )
		return false;
	ret = (*hidQueueInterface)->
		setEventCallout(hidQueueInterface,
		QueueCallbackFunction, NULL, myPrivateData);
	if ( ret != kIOReturnSuccess )
		return false;
	CFRunLoopAddSource(CFRunLoopGetCurrent(), eventSource,
		kCFRunLoopDefaultMode);
	return true;
}
