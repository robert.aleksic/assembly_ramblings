{$mode objfpc}{$H+}
{$linkframework CoreFoundation}
{$linkframework IOKit}
//{$linkframework Carbon}
program hidlib;
uses
  sysutils, //ctypes,
  kern_return, IOKitReturn,
  CFBase, CFDictionary, CFString, CFNumber, CFRunLoop,
  //CarbonEvents, MacApplication,
  //MacOSXPosix,
  //MacOSAll,

  utf,keyboard,screen; // my units

const
  maxdevices = 20;

  kHIDPage_GenericDesktop = 1;
  kHIDUsage_GD_Mouse      = 2;
  kHIDUsage_GD_Keyboard   = 6;

  kHIDPage_KeyboardOrKeypad = 7;

  kHIDUsage_KeyboardErrorRollOver  = 1;
  kHIDUsage_KeyboardPOSTFail       = 2;
  kHIDUsage_KeyboardErrorUndefined = 3;
  kHIDUsage_Keyboard_Reserved      = -1;

  HIDOPS = 0;

type
  IOHIDManagerRef = pointer;
  IOHIDDeviceRef  = pointer;
  IOHIDValueRef   = pointer;
  IOHIDElementRef = pointer;

  cfdict = CFMutableDictionaryRef;

var
  runloopmode : CFStringRef;

  hidm : IOHIDManagerRef;

  devnum  : integer;
  dev     : array [0..maxdevices-1] of record
                                         ref  : IOHIDDeviceRef;
                                         name : string
                                       end;

type
  IOHIDDeviceCallback = procedure (context : pointer; result : kern_return_t;
                                   sender  : pointer; device : IOHIDDeviceRef); cdecl;

  function IOHIDManagerCreate (alloc : CFAllocatorRef;  options : UInt32) : IOHIDManagerRef; cdecl; external;
  function IOHIDManagerOpen   (hidm  : IOHIDManagerRef; options : UInt32) : kern_return_t;   cdecl; external;
  function IOHIDManagerClose  (hidm  : IOHIDManagerRef; options : UInt32) : kern_return_t;   cdecl; external;

  procedure IOHIDManagerRegisterDeviceMatchingCallback
      (hidm : IOHIDManagerRef; cb : IOHIDDeviceCallback; context : pointer); cdecl; external;
  procedure IOHIDManagerScheduleWithRunLoop
      (hidm : IOHIDManagerRef; runloop : CFRunLoopRef; runloopmode : CFStringRef); cdecl; external;
  procedure IOHIDManagerUnscheduleFromRunLoop
      (hidm : IOHIDManagerRef; runloop : CFRunLoopRef; runloopmode : CFStringRef); cdecl; external;
  procedure IOHIDManagerSetDeviceMatching
      (hidm : IOHIDManagerRef; dict : CFDictionaryRef); cdecl; external;

  function IOHIDDeviceGetProperty (dev : IOHIDDeviceRef; key : CFStringRef) : CFTypeRef; cdecl; external;

  function IOHIDDeviceOpen (dev : IOHIDDeviceRef; options : UInt32) : IOReturn; cdecl; external;

type
  IOHIDValueCallback = procedure (var context : integer; result : kern_return_t;
                                   sender : pointer; val    : IOHIDValueRef); cdecl;

  procedure IOHIDDeviceRegisterInputValueCallback
      (dev : IOHIDDeviceRef; cb : IOHIDValueCallback; var context : integer); cdecl; external;

  procedure IOHIDDeviceScheduleWithRunLoop
      (dev : IOHIDDeviceRef; runloop : CFRunLoopRef; runloopmode : CFStringRef); cdecl; external;

  function IOHIDValueGetElement      (val : IOHIDValueRef)   : IOHIDElementRef; cdecl; external;
  function IOHIDValueGetIntegerValue (val : IOHIDValueRef)   : CFIndex;         cdecl; external;
  function IOHIDElementGetUsagePage  (el  : IOHIDElementRef) : int32;           cdecl; external;
  function IOHIDElementGetUsage      (el  : IOHIDElementRef) : int32;           cdecl; external;



  function isnil (p:pointer) : boolean;
  begin isnil := (ptruint(p) = 0) or ((not ptruint(p)) = 0) or (p=nil) end;

  function notnil (p:pointer) : boolean;
  begin notnil := not isnil (p) end;

  function strfromCFstr (cfs : CFStringRef) : string;
  var i,len : integer; ch : array [0..255] of char;
  begin
    strfromcfstr := '';
    if notnil (cfs)
    then begin
           len := CFStringGetLength (cfs); if len>255 then len := 255;
           CFStringGetCString (cfs, ch, len+1, kCFStringEncodingUTF8);
           for i := 0 to len-1 do strfromcfstr += ch[i]
         end
  end;



  procedure disposedict (var d:cfdict);
  begin
    if notnil (d)
    then begin
           CFRelease (d);
           d := nil
         end;
  end;

  procedure createdict (var d:cfdict);
  begin
    d := CFDictionaryCreateMutable (kCFAllocatorDefault,0,
                                    @kCFTypeDictionaryKeyCallBacks, @kCFTypeDictionaryValueCallBacks);
    if isnil (d)
    then d := nil
  end;

  procedure addnum (var d : cfdict; name : string; n : integer);
  var num : UInt32; ref : CFNumberRef;
  begin
    if notnil (d)
    then begin
           num := n;
           ref := CFNumberCreate (kCFAllocatorDefault, kCFNumberIntType, @num);
           if isnil (ref)
           then disposedict (d)
           else begin
                  CFDictionarySetValue (d, CFSTR(PChar(name)), ref);
                  CFRelease (ref)
                end;
         end
  end;


  function devicename (d:IOHIDDeviceRef) : string;
  var
    cfs : CFTypeRef;

  begin
    devicename := inttostr (ptruint(d));
    cfs := IOHIDDeviceGetProperty (d, CFSTR ('Manufacturer'));
    if notnil (cfs)
    then begin
           CFRetain (cfs);
           devicename += ' Manufacturer: '+strfromcfstr (cfs);
           CFRelease (cfs)
         end;
    cfs := IOHIDDeviceGetProperty (d, CFSTR ('Product'));
    if notnil (cfs)
    then begin
           CFRetain (cfs);
           devicename += ' Product: '+strfromcfstr (cfs);
           CFRelease (cfs)
         end
  end;

  procedure enumerator (ctxt   : pointer; res  : kern_return_t;
                        sender : pointer; dref : IOHIDDeviceRef); cdecl;
  begin
    if (res = kIOReturnSuccess) and (devnum < maxdevices)
    then begin
           dev[devnum].ref := dref;
           devnum := devnum + 1
         end
  end;

  procedure valhandler (var ctxt : integer; res  : kern_return_t;
                        sender   : pointer; hval : IOHIDValueRef); cdecl;
  var
    elem : IOHIDElementRef;
    val  : CFIndex;
    page, usage : int32;

    ch : char;

  begin
    if res = kIOReturnSuccess
    then begin
           elem := IOHIDValueGetElement      (hval);
           val  := IOHIDValueGetIntegerValue (hval);
           writeln ('value: ',val);

           page  := IOHIDElementGetUsagePage (elem);
           usage := IOHIDElementGetUsage     (elem);

           // leftfn (page = 255, usage = 3, up/dn)

           if (page = kHIDPage_KeyboardOrKeypad)
           then if (usage > kHIDUsage_KeyboardErrorUndefined) and
                   not ((usage = kHIDUsage_Keyboard_Reserved) and (val=0))
                then begin

                       write (ptruint(sender):2);
                       if (usage<=29) and (usage>=4)
                       then write (chr(usage-4+ord('a')):4,' ')
                       else write (usage:4,' ');

                       case val of
                         0  :  write ('Up');
                         1  :  write ('Dn');
                         else  write ('??')
                       end;
                       writeln
                     end

         end
  end;

  procedure disposehidman;
  begin
    if notnil(hidm)
    then begin
           IOHIDManagerClose (hidm, HIDOPS);
           CFRelease (hidm)
         end
  end;

  procedure createhidman (vendor,product : integer);
  var
    d : cfdict;

    procedure confighidman;
    var runl : CFRunLoopRef; ctxt, i : integer;
    begin
      runl := CFRunLoopGetMain; //Current; //Main?

      IOHIDManagerRegisterDeviceMatchingCallback (hidm, @enumerator, nil);
      IOHIDManagerScheduleWithRunLoop (hidm, runl, runloopmode);
      IOHIDManagerSetDeviceMatching (hidm, d);

      IOHIDManagerOpen (hidm, HIDOPS);
      while CFRunLoopRunInMode (runloopmode, 0, true) = kCFRunLoopRunHandledSource do ;

      IOHIDManagerRegisterDeviceMatchingCallback (hidm, nil, nil);
      IOHIDManagerUnscheduleFromRunLoop (hidm, runl, runloopmode);

      for i := 0 to devnum-1 do
        with dev[i] do
          if IOHIDDeviceOpen (ref,HIDOPS) <> kIOReturnSuccess
          then ref := nil
          else begin
                 ctxt := i;
                 IOHIDDeviceRegisterInputValueCallback (ref, @valhandler, ctxt);
                 IOHIDDeviceScheduleWithRunLoop (ref, runl, runloopmode);
               end

    end;

  begin
    hidm := IOHIDManagerCreate (kCFAllocatorDefault, HIDOPS);
    if notnil (hidm)
    then begin
           createdict (d);
           addnum (d,'DeviceUsagePage', vendor);
           addnum (d,'DeviceUsage',     product);
           if notnil (d)
           then confighidman
           else begin
                  disposehidman;
                  hidm := nil
                end;
           disposedict (d)
        end
  end;


  procedure pollkey;
  begin
    while CFRunLoopRunInMode (runloopmode, 0, true) = kCFRunLoopRunHandledSource do
  end;

var i : integer; ch : string;
begin
  kbinit;


  runloopmode := CFSTR ('HIDlib kbd');

  devnum := 0;
  createhidman (kHIDPage_GenericDesktop, kHIDUsage_GD_Keyboard);
  if notnil (hidm)
  then begin

         for i := 0 to devnum-1 do
           with dev[i] do
             if ref<>nil then writeln (devicename (ref));

         //CFRunLoopRun;
         //CFRunLoopWakeUp (CFRunLoopGetCurrent);

         repeat
           pollkey; writeln;
           waitch (ch); writeln ('key: ',ch);
         until ch='q';

         disposehidman
       end;

  kbdone
end.