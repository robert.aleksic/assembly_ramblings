{$mode objfpc} {$longstrings on}
program rawmodetest;
uses
  sysutils, baseunix, termio, screen;

{$ifndef _POSIX_VDISABLE}
const
  _POSIX_VDISABLE = $ff;
  VDSUSP=11;
{$endif}

var
  x,y : integer;
  ch  : char;

type
  speckeys = (knone, kleft, kright, kup, kdown,
              khome, kend, kpgup, kpgdn,
              kf1, kf2, kf3, kf4,
              kf5, kf6, kf7, kf8,
              kbksp, kesc, kent, ktab);
  r = record s:string; sk:speckeys end;

const
  speccode = $e0;

  k_left = #1; k_right = #2;

  enc1 : array [1..4] of r = (
    (s:#127; sk : kbksp),
    (s:#027; sk : kesc),
    (s:#013; sk : kent),
    (s:#009; sk : ktab));

  enc2 : array [1..10] of r = (
    (s:'OD'; sk : kleft),
    (s:'OC'; sk : kright),
    (s:'OA'; sk : kup),
    (s:'OB'; sk : kdown),
    (s:'OH'; sk : khome),
    (s:'OF'; sk : kend),
    (s:'OP'; sk : kf1),
    (s:'OQ'; sk : kf2),
    (s:'OR'; sk : kf3),
    (s:'OS'; sk : kf4));

  enc3 : array [1..2] of r = (
    (s:'[5~'; sk : kpgup),
    (s:'[6~'; sk : kpgdn));

  enc4 : array [1..4] of r = (
    (s:'[15~'; sk : kf5),
    (s:'[17~'; sk : kf6),
    (s:'[18~'; sk : kf7),
    (s:'[19~'; sk : kf8));


{var
  OldIO : termio.termios;

  inputRaw, outputRaw: boolean;

  procedure saveRawSettings (const tio: termio.termios);
  begin
    with tio do
    begin
      inputRaw := ((c_iflag and (IGNBRK or BRKINT or PARMRK or ISTRIP or
                                 INLCR  or IGNCR  or ICRNL  or IXON)) = 0) and
                  ((c_lflag and (ECHO or ECHONL or ICANON or ISIG or IEXTEN)) = 0);
      outputRaw := ((c_oflag and OPOST) = 0) and
                   ((c_cflag and (CSIZE or PARENB)) = 0) and
                   ((c_cflag and CS8) <> 0)
     end
  end;

  procedure restoreRawSettings (tio: termio.termios);
  begin
    with tio do
    begin
      if inputRaw
      then begin
             c_iflag := c_iflag and not (IGNBRK or BRKINT or PARMRK or ISTRIP or
                                          INLCR or IGNCR or ICRNL or IXON);
             c_lflag := c_lflag and not (ECHO or ECHONL or ICANON or ISIG or IEXTEN)
           end;
      if outPutRaw
      then begin
             c_oflag := c_oflag and not OPOST;
             c_cflag := c_cflag and not (CSIZE or PARENB) or CS8
           end
    end
  end;


  procedure SetRawMode(b:boolean);
  var Tio : Termios;
  begin
    if b
    then begin
           TCGetAttr (1,Tio);
           SaveRawSettings (Tio);
           OldIO := Tio;
           CFMakeRaw (Tio)
         end
    else begin
           RestoreRawSettings (OldIO);
           Tio := OldIO;
         end;
    TCSetAttr (1,TCSANOW,Tio)
  end;

}
var
  tio    : termios;
  i, num : int64;
  buff   : array [1..100] of char;

begin
  init;
  // SetRawMode (true);

  TCGetAttr (0,Tio);
  //CFMakeRaw (Tio); // Tio.c_lflag := tio.c_lflag and not ICANON;

  tio.c_iflag := tio.c_iflag and not (IXON or INLCR or ICRNL);
  tio.c_lflag := tio.c_lflag and not (ICANON or ECHO or IEXTEN);

  tio.c_cc [VMIN]  := 1;
  tio.c_cc [VTIME] := 0;

  tio.c_lflag := tio.c_lflag and not ISIG;
  tio.c_cc [VQUIT]  := _POSIX_VDISABLE;
  tio.c_cc [VSUSP]  := _POSIX_VDISABLE;
  tio.c_cc [VDSUSP] := _POSIX_VDISABLE;

  TCSetAttr (0,TCSANOW,Tio);

  repeat

    repeat
      sleep (10);
      fpioctl (stdinputhandle,FIONREAD,@num)
    until num<>0;
    write (num,' ');

    fpread (stdinputhandle,@buff,num);

    for i := 1 to num do
    begin
      ch := buff[i];
      if ord(ch) in [32..126]
      then write (ch,' ')
      else if ch = #27
           then write ('esc ')
           else write (ord(ch),' ')
    end;
    writeln

  until ord (ch) = 113;

  //SetRawMode (false);
  done
end.