In order to write decent development system one needs decent editor.
In order to write decent editor one must be able to manipulate screen and keyboard.

As of screen manipulation we can settle for ansi terminals and use esc sequences. Good thing is that it can be used over ssh and it can be easily made multiplatform.

As of keyboard this is where things start to get complicated. If we use ncourses we have large dependance which is not as much of a problem as it is the fact that we can not easily discern between say ctrl-m and enter, then esc detection becomes complicated on various os's (linux console and linux termina for example) etc.

So first experiment will be to create simple keyboard unit which can be platform independent and can detect keys pressed and released...

