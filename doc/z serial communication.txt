on serial communication
-----------------------

it might seem worthwhile to pursue some sort of simple peer to peer protocol for serial communication.

Problems with existing protocols are as follows:
- RS232/422 - to slow, to many wires
- USB - not p2p, asymmetric, to complicated in software/hardware on both sides
- Ethernet (IP) - to many wires, to complicated in software/hardware on both sides
- i2c - asymmetric (master/slave), also to complicated
- sbus - not p2p, again to complicated

First things that come to mind are similar to transputer OS and DS links.
OS is 5/20Mhz (8/12+- bandwidth), DS is differential with 100MHz+.

OS Links
--------
- bidirectional (two unidirectional channels)
- each channel carry data and acknowledge for other channel

- operating frequency 5/20MHz
- effective throughput - 1.8MB/sec in one direction, 2.4MB/sec in both directions

- asynchronous

- cabling - two wires / twisted pair / two twisted pairs + screening / + sreeening for pairs

- up to 30 cm - direct conection
- up to 10 m - series termination
- up to 20 m - buffers
- up to 30 m - (only 5 and 10MHz) diferential rs 422
- over 30m - optics ?

- packets: data: (11 bits - start bit  - 1, data bit - 1, 8 bits of data, stop bit - 0),
    acknowledge: (2 bits, start bit - 1, and stop bit - 0)

- acknowledge can be sent as soon as receiver get first bit to avoid delay in streaming data
    it can be late on first packet, but as soon as receiver is sure it is synchronised he can send it after start bits are processed
- single byte buffer on both sides
- live connection/disconnection handling

DS Links
--------
- bidirectional, synchronous (two unidirectional channels)
- operating frequency 100MHz
- bidirectional, sinchronous
- eight wires (diferential )
- data and strobe per direction
- clock is extracted from data xor strobe
- evolved to some ieee standard and then to spacelink
- there are open source vhdl's
