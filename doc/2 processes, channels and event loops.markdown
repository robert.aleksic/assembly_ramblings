thinking on processes, channels and event loops
-----------------------------------------------

Parallel processing is inherently hard. Sequentional processes are much easier.

Each process do some sort of processing and service channels at the same time. If all processes can be represented like this:

      stop, error := false, false
      WHILE NOT stop AND NOT error DO
        ALT [i=0 FOR n]
          guard[i] & chan[i]? val
            process[i] (val, failed[i])
          ctrl.in ? any
            stop := true
          failed[i] ? any
            SEQ
              error := true
              ctrl.out ! i; err.code


then it might be possible to do some main event loop processing to service all channels and maybe also to add dynamic elements. Simmilar stuff is done in erlang, although paying high penalty in terms of execution time.

Dynamism is achieved by processes serving queues. Above mentioned process runs forever unless stopped or one of the processes have ended in error state. Should we have option not to stop on every error, and stop when all processes have completed? duno

Ok, lets say that we have execution engine.

We send it processes to be run and have been informed when some process copleted execution, or ended in error state and we have possibility to stop processes. Seems that this engine could be usefull if we can somehow abstract processes so that we  don't depend on different hardware.

- hardware id
- process compiler for given hardware id
- execution engine is (somehow) sent to hardware and it starts.
- we can then on-the-fly compile processes for given hardware and send (again somhow) as binary to be executed
- what is hardware id, what is expected from execution engine, how to handle errors etc. ?
- is there something that underlining hardware must support in order for this model to function properly ?

----------------
control networks
----------------

There are hardware nodes, uniquily numbered.

There is control network. It is composed from hardware and each and of them is connected via control channels (in CSP sense) named ctrl.up and ctrl.down. Global names for them are ctrl.up[i] and ctrl.down[i]

Each message consists of address and content.
There is master node called server.

Every node either consume message addressed to it and send back acknowledge ot server, or forward message to other channel.
Nodes can be circulary connected to be fault tolerant to one failure.

If only server -> node and slave -> node messages are communicated through this network then management can be centralized, but if server fails communication network is not operable.

Server:
  send message in one direction to node
  wait for acknowledge
  if acknowledge fails send it to other direction
  wait for acknowledge
  if other acknowledge fails

Node:
  same sa server?

Node should have means to indruce themselfs to server when connected
Servers should have map of connected nodes and their status and be able to reconfigure on failure/repair
Nodes should be able to be disconected, repaired/upgraded with or without bridging them and connected back to control network
auto reset on error?

Control messages:
  - node introduce
  - node status
  - node set number
  - node stop
  - node reset
  - node send program and start
  - node inquiry

Channels
--------
Byte based with acknowledge on hardware level
Only one byte buffer possibly efficiently immediately received by procces on the other end with overlapped acknowledge
Timers and signals as local channels (thought one to many)

Higher level - array based - len::data
             - end of message based - data;eom

Failure modes, timed response, alternative routes, diconnects?

 Channel hardware:

     ch.out ! x
       1. buffer x; if NOT ch.empty; deschedule
       2. reschedule hardware sends buffer when other party is ready
       3. on out.ack out.empty := true; reschedule
     ch.in ? x
       1.
       2. x := buff send ack

 Software channels (TBD)

     ch.out ! x
      ...