What does it mean to develop software, hardware and combined systems?
---------------------------------------------------------------------

Specify what are you doing and why, what problem are you solving. Refine. Prototype. Repeat. Document decisions. Revise. Repeat. Throw it away and redo. Write a lot (with back propagation and rewriting from scratch).

On tools
--------

- Hardware ?
- Software (programming language, development system)
  - avoid complicated stuff (languages, editors, libraries, os's, so called tool chains etc.)
- Avoid debugging whenever possible, writing on screen or in file is often enough
- Assert often

Assembler
---------

- Make your own
- Make disassembler and emulator, you'll learn a lot
- You don't have to use all instructions, select reasonable subset
- You can always improve assembler syntax aiming at readability and re-usability of assembly code
  - think  constants, variables, func/proc, modules (not textual includes, c proved that it is bad idea)
- 'Compile all' works nice, no need yet to complicate by libraries, versions and separate compilation
- While assembling/compiling, stop on first error, don't try to recover. Be as precise in error messages and their location

Good development system
-----------------------

- Nice editor, single keystroke to assembly/compile/build and/or run.
- Should work with emulator or real hardware
- Think raspberry pi, if you have to write to sd card to run program, you'll be spending more time on copying and swapping card (destroying connectors while doing it), then on writing program.
- Should be simple to setup (think not eclipse)

Editor
------

  - should handle utf-8
  - should have ability to highlight words (by underline or italic for example), characters should be mono spaced

  commands:
    bot,eot,bol,eol,insert,prevw,nextw,
    del,bksp,mark,exchg,cut,paste,non persistent clipboard,undelete immediately after deletes
    left,right,up,down,pageup,pagedown,fread,fwrite,whigh

      procedure editor (var mem : array [0..len-1] of char; var lo,hi:integer; var exitchars: set of char;
                        command_keys : array [command] of ctrl_char; fg,bg : color; folder : string; var exitch : ctrl_char;
                        ?var redraw : boolean; message : string);

   - lo is location for next char
   - text before cursor is in mem[0..lo-1], text on cursor is in mem[hi] .. len-1
   - free memory is lo .. hi-1
   - mark can be anywhere where cursor is
   - selection is part of text between mark and cursor

  empty text                    - lo=0, hi=len-1

  mark?file_name;fread          - read file
  bot;mark;eot!file_name;fwrite - write selection to file
  mark?fread                    - read directory

  editor is extended by calling it, checking exitch, modifying text or whatever and recalling it again

  main loop in development system:

      target := emulator;
      quit := false;
      while not quit do
      begin
        editor (...,exitch);
        case exitch of

          chgtarget : if target<>emulator
                      then target := emulator
                      else target := remote;

          compile, run : begin
                           do_compile (err,pos,message);
                           if err
                           then setloandhi
                           else if exitch=compile
                                then message := 'compiled and saved to aout'
                                else begin
                                       clrscr;
                                       connectscrandkeyboard;
                                       if target=remote
                                       then runemulator ('aout')
                                       else transfertotargetandrun ('aout');
                                       disconnectscrandkeyboard
                                     end
                         end;
          others skip
        end
      end

Channel abstractions
--------------------

- Channel abstraction should be provided by underlying software in emulator/os and on boot software on remote hardware
- Control channel (ctrl), Link(s) l0,l1,l2 ...

   ctrl.out/in on development system connected to ctrl.in/out on hardware or emulator.

   connections (terminal to link <=> terminal.keyboard -> link.in; terminal.screen -> link.out)
     devsys.terminal to devboard.ctrl (to enter commands)
     after run devsys.terminal reconnect to devboard.link, after stop reconnects back to ctrl.

  devsys:
    ctrl.out! reset | send;prog | run | peek;addr;value | poke;addr | stop | status | config?

    while true
      ctrl.in? messages

  terminal window for ctrl link in dev sys?


Remote system have control channel (remote system could be real hardware or emulator)
