
   Back to basics or what am I concerned about as a computer engineer?
Why consciousness in making design decisions and documenting them matters.
==========================================================================

It seems that size and complexity of the computer systems is rapidly increasing both in hardware and software.

Quality of documentation of those systems is such that it is much more leaning toward explaining usage and omits explaining design decisions made during development. Shear amount of this 'documentation' is overwhelming.

Basic things like reading keyboard, mouse and putting pixels on screen, were simple and flexible forty years ago. Today they are overcomplicated in hardware and in operating systems, with various excuses.

Operating systems become bloated and while striving for compatibility their design introduced large penalty in efficiency, predictability and correctness. It was advocated that those loses will be compensated by faster hardware, but it turned out that newer hardware also grow in complexity and follow the steps of software development.

Now, even simplest tasks on modern hardware in assembly are complex and usually have its own shortcomings which are supposed to be overcomed in software. (take for example rs232 vs. USB, or VGA vs. HDMI). Therefore even basic I/O staff is drowning in complexity, not to mention licensing requirements.

Design decisions related to major modern processors development are hidden behind intellectual property floscule. It seems that those decisions are reactive afterthought and that only market forces are driving the development. Unfortunately those forces are bad designers and lead to bloat everywhere. Just compare contemporary processors to contemporary societies (of all varieties) and it will be obvious that both are dysfunctional in a same way and due to similar reasons.

Question is - can things be simplified and can software and hardware can become easier to develop and can design decisions be better documented?

As early as 1968 on the conference on software engineering lot of issues with development were mentioned and it seems that now in 2020 almost none of those were successfully resolved. It is a pity for engineering discipline to advance, for over 50 years, in increasing entropy instead of decreasing it by, at least, structuring design.

It seem obvious that hardware/software, os/utility software and similar dichotomies are artificial and that without properly understanding and designing systems from the bottom up, we will be unable to manage complexity which we have produced.

Bad bad education
-----------------

Current state of affairs in computer science resulted in poor eduction of new generation of engineers, which further deteriorate CS. This vicious circle have to be broken if we want CS to become proper science. Contrary to CS, in physics, for example, it is unthinkable that quantum physicist have no knowledge of basic laws of physics. In computer science it is norm that software engineer does not know basic data structures representation and operations on them on hardware level. Heck, in python there is no plain array as data structure. It is implemented as some sort of a list. Given wide usage on python, how will programmers learn to properly and efficiently (in terms of time and storage) use simple linear data structure such as array.

Solutions?
----------
- see transputers 1985-1990 and project oberon 1980-2013 as examples
