This is repository for _development programme_. It is named asm_ramblings and it should be renamed as soon as we come to better name.

It is called programme since it is basically collection of interwoven projects, not a single project. In standard management terminology project is set of activities bounded in time resulting in clearly defined output or failure. Programme, on the other hand, is more long term strategically oriented set of activities with more vaguely defined goals. So we will refer to it as _programme_ in the future.

Goals of programme
------------------
Programme started when myself (robert) and dejan meet in august 2019, and realized that we are thinking about same problems but from different perspectives - programming, engineering education, unnecessary complicated software and hardware and most of all, inability to easily assemble teams of engineers (for the lack of better word) capable to quick start development of projects related to decently designed and well documented (therefore better maintainable) software and/or hardware. My perspective is coming experience in software development, and dejan's is similar, but with lot more of academy and entrepreneurship experience in industrial settings. Since we haven't been in contact for more than twenty years we were both eager to start working together on something and this programme is the result of that eagerness.

Goals set so far are quite simple - to experiment in software and hardware aiming to produce components of simpler development environment. Since dejan is vivid collector of vintage equipment from late twenty century he is currently working on z80 educational hardware (documented in folder hardware), while I focus more on setting up wider development framework.

Themes we have so far touched upon are: assemblers, communication and development system itself. Since we are currently focusing on smaller experiments we haven't yet set any precise targets. We expect to develop documentation methodology in this early stages and that is as tangible as it got so far.

The programme is developing on it's own pace, dictated by amount of time we have to devote to it, with aim to thoroughly document design decisions in similar manner to what have been done in transputing and oberon system projects.

Folders
-------
- doc - this is where all more or less structured general thoughts are put
- hardware - for hardware side of the project (includes relevant docs in doc folder)
- software - for software side of the project (includes relevant docs in doc folder)
- processors - info on various real and virtual processors (one page cpu and hex)

Software
--------
Development environment components selected so far are free pascal compiler and sublime-text editor, due to their simplicity and availability on most of contemporary OS's and hardware.

Historical sources
------------------
Our thinking is greatly influenced by work of Hoare (CSP), Dijkstra (axiomatic programming), Wirth (Pascal and Oberon system) and David May (OCCAM, transputers and hex assembly, development documentation), as well as little known project originated in mid 1980's in former Yugoslavia - VUK (educational computer and OS, based on MC 68000) developed by group of students and engineers from cities of Pirot, Leskovac and Niš.

References and literature
-------------------------
1. EWD notes
2. Structured programming
3. Pascal and Oberon system related literature (ETZH, Wirth's site, Project Oberon)
4. CSP related literature ()
5. transputer related literature (transputer.net, David May's site, etc.)
6. one page cpu (https://revaldinho.github.io/opc)

...

License
-------
Anyone is free to do whatever he like with this materials, except where bounded with other licenses.

All comments are welcome (you can use bug reports for now) and we are open for anyone who wish to join us in this exploration.
